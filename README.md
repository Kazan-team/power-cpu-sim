# Building on Linux (common)

1. Install Rust. You can get it [here](https://rustup.rs/).

2. Determine which version LLVM your version of Rust uses:

    ```bash
    rustc -vV
    ```
    
    On my computer that outputs:
    ```
    <elided stuff>
    LLVM version: 13.0.0
    ```

3. Install a matching version of Clang:

    For Ubuntu and other Debian-based Linux OSes, replacing the 2 instances of `13` with the version of LLVM you need:

    ```bash
    sudo apt update
    sudo apt install clang-13 llvm-13-tools
    ```

    If that doesn't work, you may need to add [LLVM's APT repositories](https://apt.llvm.org/) to your APT `sources.list`:

    First, add LLVM's GPG key to APT:

    ```bash
    wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key | sudo apt-key add -
    ```

    Then, edit your APT `sources.list`:

    ```bash
    sudo nano /etc/apt/sources.list
    ```

    Next, add the following lines at the end, replacing the 2 instances of `13` with the version of LLVM you need and the 4 instances of `bionic` with the codeword for your OS version:

    ```
    deb http://apt.llvm.org/bionic/ llvm-toolchain-bionic-13 main
    deb-src http://apt.llvm.org/bionic/ llvm-toolchain-bionic-13 main
    ```

    Save and exit Nano.

    Rerun the `apt update` and `apt install` commands at the top of step 3.

## Building for Linux

You need to follow the steps under [Building on Linux (common)](#building-on-linux-common) first.

4. Install libc++.

    You need to install the version of libc++ that matches the version of LLVM your version of Rust uses.

    For Ubuntu and other Debian-based Linux OSes, replacing the 2 instances of `13` with the version of LLVM your version of Rust uses:

    ```bash
    sudo apt update
    sudo apt install libc++abi-13-dev libc++-13-dev
    ```

5. Build the code:

    To build in debug mode, run:
    ```bash
    cargo build
    ```
    The output executable is `./target/debug/power_cpu_sim`

    To build in release mode, run:
    ```bash
    cargo build --release
    ```
    The output executable is `./target/release/power_cpu_sim`

## Building for the Web

You need to follow the steps under [Building on Linux (common)](#building-on-linux-common) first.

4. Download the WASI sysroot. You can get it [here](https://github.com/WebAssembly/wasi-sdk/releases/download/wasi-sdk-14/wasi-sysroot-14.0.tar.gz).

5. Extract it into the root directory of the git repo:

    ```bash
    tar -xaf <path-to>/wasi-sysroot-14.0.tar.gz
    ```

5. Build the code:

    To build in debug mode, run:
    ```bash
    cargo build --target=wasm32-wasi
    ```

    To build in release mode, run:
    ```bash
    cargo build --target=wasm32-wasi --release
    ```

6. Try it out on a simple demo server (optional):

    You need to build the code in debug mode (see step 5) and install libc++ (see step 4 of [Building for Linux](#building-for-linux)), then run:
    ```bash
    cargo run --example=dev-server
    ```

    You can then go to [`localhost:8080`](http://localhost:8080/).

7. Install it to a web server:

    Replace `/var/www/html` by the path to some directory that your web server
    serves from. You need to have built the code in release mode (see step 5).
    ```bash
    sudo install -m644 index.html Notices.txt target/wasm32-wasi/release/power_cpu_sim.wasm /var/www/html/
    ```

    Ensure the web server serves the `.wasm` file using mime type
    `application/wasm`, otherwise the startup time will be appreciably greater.
