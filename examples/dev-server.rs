use warp::Filter;

#[tokio::main]
async fn main() {
    let index = warp::get()
        .and(warp::path::end())
        .and(warp::fs::file("./index.html"));
    let wasi = warp::get()
        .and(warp::path("power_cpu_sim.wasm"))
        .and(warp::fs::file(
            "./target/wasm32-wasi/debug/power_cpu_sim.wasm",
        ));
    let notices = warp::get()
        .and(warp::path("Notices.txt"))
        .and(warp::fs::file("./Notices.txt"));

    let routes = index.or(wasi).or(notices);

    warp::serve(routes).run(([127, 0, 0, 1], 8080)).await;
}
