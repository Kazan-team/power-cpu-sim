// SPDX-License-Identifier: LGPL-2.1-or-later
// See Notices.txt for copyright information
#pragma once

#include "arc.h"
#include "instruction.h"
#include "instruction_state.h"
#include "pipe.h"
#include "simulator.h"
#include <concepts>
#include <functional>
#include <memory>
#include <optional>
#include <type_traits>
#include <vector>

class SchedulerPipe : public Pipe {
public:
    struct ExecutionPortInfo final {
        Arc<const PipeFactory> execution_pipe_factory;
        // nullopt indicates this port is dynamically expandable
        std::optional<std::size_t> pipe_count;
    };
    friend class ExecutionPort;
    class ExecutionPort final {
    public:
        struct PipeData final {
            Arc<Pipe> pipe;
            Arc<InputPipe> input_pipe;
        };

    private:
        ExecutionPortInfo m_info;
        std::vector<PipeData> m_pipes;

    public:
        const ExecutionPortInfo &info() const noexcept {
            return m_info;
        }
        std::span<const PipeData> pipes() const noexcept {
            return m_pipes;
        }
        /// append a new pipe.
        /// if this port is not dynamically expandable returns false without
        /// changing anything, otherwise appends the new pipe and returns true.
        bool try_append_new_pipe(simulator::Simulator &simulator) {
            if (info().pipe_count.has_value())
                return false;
            do_append_new_pipe(simulator);
            return true;
        }

    private:
        void do_append_new_pipe(simulator::Simulator &simulator) {
            auto input_pipe = static_arc_cast<InputPipe>(
                InputPipe::make_factory()->create(nullptr, simulator));
            auto pipe =
                info().execution_pipe_factory->create(input_pipe, simulator);
            m_pipes.push_back({.pipe = pipe, .input_pipe = input_pipe});
        }

    public:
        explicit ExecutionPort(ExecutionPortInfo info_in,
                               simulator::Simulator &simulator)
            : m_info(std::move(info_in)), m_pipes() {
            if (info().pipe_count) {
                std::size_t pipe_count = *info().pipe_count;
                m_pipes.reserve(pipe_count);
                for (std::size_t i = 0; i < pipe_count; i++) {
                    do_append_new_pipe(simulator);
                }
            }
        }
        ExecutionPort(const ExecutionPort &) noexcept = delete;
        ExecutionPort &operator=(const ExecutionPort &) noexcept = delete;
        ExecutionPort(ExecutionPort &&) noexcept = default;
        ExecutionPort &operator=(ExecutionPort &&) noexcept = default;
    };

private:
    std::vector<ExecutionPort> m_execution_ports;

public:
    explicit SchedulerPipe(
        Arc<Pipe> input_pipe, Arc<const PipeFactory> factory,
        simulator::Simulator &simulator,
        std::span<const ExecutionPortInfo> execution_port_infos)
        : Pipe(std::move(input_pipe), std::move(factory), simulator),
          m_execution_ports() {
        m_execution_ports.reserve(execution_port_infos.size());
        for (auto &info : execution_port_infos) {
            m_execution_ports.emplace_back(info, simulator);
        }
    }
    std::span<const ExecutionPort> execution_ports() const noexcept {
        return m_execution_ports;
    }
    std::span<ExecutionPort> execution_ports() noexcept {
        return m_execution_ports;
    }
};