// SPDX-License-Identifier: LGPL-2.1-or-later
// See Notices.txt for copyright information
#pragma once

#include "c_api.h"
#include <algorithm>
#include <cassert>
#include <concepts>
#include <cstdint>
#include <functional>
#include <initializer_list>
#include <limits>
#include <map>
#include <optional>
#include <set>
#include <span>
#include <string>
#include <string_view>
#include <type_traits>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <variant>
#include <vector>

namespace config_parser {
struct ParseFailure final {
    std::string message;
    std::string field_name;
    explicit ParseFailure(std::string message,
                          std::string field_name = "") noexcept
        : message(std::move(message)), field_name(std::move(field_name)) {}
    static std::string get_message_string_for(TomlValue toml) {
        using Kind = TomlValue::Kind;
        switch (toml.kind()) {
        case Kind::Integer:
            return "integer `" + std::to_string(toml.get_integer()) + "`";
        case Kind::String:
            return "string \"" + std::string(toml.get_string()) + "\"";
        case Kind::Float:
            return "floating point `" + std::to_string(toml.get_float()) + "`";
        case Kind::Boolean:
            return toml.get_boolean() ? "boolean `true`" : "boolean `false`";
        case Kind::Datetime:
            return "TOML datetime \"" + std::string(toml.get_datetime()) + "\"";
        case Kind::Array:
            return "sequence";
        case Kind::Table:
            return "map";
        }
        assert(false);
    }
    static ParseFailure for_missing_field(std::string_view field_name,
                                          std::string parent_field_name = {}) {
        std::string message = "missing field: `";
        message += field_name;
        message += "`";
        return ParseFailure(message, parent_field_name);
    }
    static ParseFailure for_invalid_type(TomlValue toml, std::string field_name,
                                         std::string_view expected_type) {
        std::string message = "invalid type: " + get_message_string_for(toml);
        message += ", expected a ";
        message += expected_type;
        return ParseFailure(message, field_name);
    }
    static ParseFailure for_invalid_value(TomlValue toml,
                                          std::string field_name,
                                          std::string_view expected_type) {
        std::string message = "invalid value: " + get_message_string_for(toml);
        message += ", expected a ";
        message += expected_type;
        return ParseFailure(message, field_name);
    }
    static ParseFailure for_expected_nonzero_value(std::string field_name) {
        return ParseFailure("expected a non-zero value", field_name);
    }
    template <typename T>
    static ParseFailure for_unknown_variant(
        std::string_view toml_string, std::string field_name,
        std::span<const std::pair<T, std::string_view>> variants) {
        std::string message = "unknown variant: `";
        message += toml_string;
        switch (variants.size()) {
        case 0:
            message += "`, there are no variants";
            break;
        case 1:
            message += "`, expected `";
            message += variants[0].second;
            message += "`";
            break;
        case 2:
            message += "`, expected `";
            message += variants[0].second;
            message += "` or `";
            message += variants[1].second;
            message += "`";
            break;
        default: {
            message += "`, expected one of `";
            auto separator = "";
            for (auto &variant : variants) {
                message += separator;
                separator = "`, `";
                message += variant.second;
            }
            message += "`";
        }
        }
        return ParseFailure(message, field_name);
    }
    static ParseFailure
    for_unknown_field(std::string_view unknown_field_name,
                      std::span<const std::string_view> fields,
                      std::string_view parent_field_name) {
        std::string message = "unknown field: `";
        message += unknown_field_name;
        switch (fields.size()) {
        case 0:
            message += "`, there are no fields";
            break;
        case 1:
            message += "`, expected `";
            message += fields[0];
            message += "`";
            break;
        case 2:
            message += "`, expected `";
            message += fields[0];
            message += "` or `";
            message += fields[1];
            message += "`";
            break;
        default: {
            message += "`, expected one of `";
            auto separator = "";
            for (auto &field : fields) {
                message += separator;
                separator = "`, `";
                message += field;
            }
            message += "`";
        }
        }
        return ParseFailure(message, std::string(parent_field_name));
    }
    void insert_parent_field_name(std::string_view parent_field_name) {
        if (parent_field_name.empty())
            return;
        if (field_name.empty())
            field_name = parent_field_name;
        else {
            field_name.insert(0, ".");
            field_name.insert(0, parent_field_name);
        }
    }
    std::string to_string() const {
        auto retval = this->message;
        if (!field_name.empty()) {
            retval += " for key `";
            retval += field_name;
            retval += "`";
        }
        return retval;
    }
    [[noreturn]] void print_and_exit() const;
};

template <typename T>
using ParseResult = std::variant<T, ParseFailure>;

namespace detail {
template <typename T, typename = void>
struct DeduceParserFor {};
} // namespace detail

template <typename T>
struct ParserFor final : public detail::DeduceParserFor<T> {};

template <typename T>
concept Parsable = std::movable<T> &&
    requires(std::optional<TomlValue> toml, std::string_view field_name) {
    { ParserFor<T>::parse(toml, field_name) } -> std::same_as<ParseResult<T>>;
};

template <typename Class, typename F>
struct Field final {
    constexpr Field(F Class::*field, std::string_view name) noexcept
        : field(field), name(name) {}
    F Class::*field;
    std::string_view name;
};

namespace detail {
template <typename Class, Parsable... Fields>
std::true_type fields_deduce(Field<Class, int>,
                             std::tuple<Field<Class, Fields>...>);
} // namespace detail

template <typename Fields, typename Class>
concept FieldsFor = requires(Field<Class, int> field1, Fields fields) {
    { detail::fields_deduce(field1, fields) } -> std::same_as<std::true_type>;
};

template <typename T>
concept HasFieldsConst = requires {
    { T::FIELDS } -> FieldsFor<T>;
};

template <typename T>
concept HasNameConst = requires {
    { T::NAME } -> std::same_as<const std::string_view &>;
};

template <typename T>
concept HasParseFn = requires(std::optional<TomlValue> toml,
                              std::string_view field_name) {
    { T::parse(toml, field_name) } -> std::same_as<ParseResult<T>>;
};

namespace detail {
    template <typename T>
    concept ParsableStruct = std::movable<T> && std::default_initializable<T> &&
                             HasFieldsConst<T> && !HasParseFn<T> &&
                             HasNameConst<T>;

    template <std::movable T>
    requires std::movable<T> &&
        requires(std::optional<TomlValue> toml, std::string_view field_name) {
        { T::parse(toml, field_name) } -> std::same_as<ParseResult<T>>;
    }
    struct DeduceParserFor<T, void> {
        static ParseResult<T> parse(std::optional<TomlValue> toml,
                                    std::string_view field_name) {
            return T::parse(toml, field_name);
        }
    };
} // namespace detail

template <>
struct ParserFor<bool> final {
    static ParseResult<bool> parse(std::optional<TomlValue> toml,
                                   std::string_view field_name) noexcept {
        if (!toml)
            return ParseFailure::for_missing_field(field_name);
        if (toml->is_boolean())
            return toml->get_boolean();
        return ParseFailure::for_invalid_type(*toml, std::string(field_name),
                                              "boolean");
    }
};

static_assert(Parsable<bool>);

template <>
struct ParserFor<std::string> final {
    static ParseResult<std::string> parse(std::optional<TomlValue> toml,
                                          std::string_view field_name) {
        if (!toml)
            return ParseFailure::for_missing_field(field_name);
        if (toml->is_string())
            return std::string(toml->get_string());
        return ParseFailure::for_invalid_type(*toml, std::string(field_name),
                                              "string");
    }
};

static_assert(Parsable<std::string>);

namespace detail {
// do UTF-8 conversion manually since I can't find a decent C++ standard
// library utf-8 decoder that isn't deprecated in C++20 or just not
// implemented.
struct ParseUTF8CodepointResults final {
    char32_t ch;
    std::string_view rest;
};

constexpr ParseUTF8CodepointResults
parse_utf8_codepoint(std::string_view text) noexcept {
    assert(!text.empty() && "missing UTF-8 codepoint");
    std::uint8_t ch;
    auto next = [&] {
        assert(!text.empty() && "truncated UTF-8 sequence");
        ch = text.front();
        text.remove_prefix(1);
    };
    next();
    std::uint32_t retval;
    if (ch & 0x80) {
        assert(ch >= 0xC2 && "invalid UTF-8 leading byte");
        std::size_t continuation_bytes;
        if ((ch & 0xE0) == 0xC0) {
            retval = ch & 0x1F;
            continuation_bytes = 1;
        } else if ((ch & 0xF0) == 0xE0) {
            retval = ch & 0xF;
            continuation_bytes = 2;
        } else {
            assert(ch <= 0xF4 && "invalid UTF-8 leading byte");
            retval = ch & 0x7;
            continuation_bytes = 3;
        }
        for (std::size_t i = 0; i < continuation_bytes; i++) {
            next();
            assert((ch & 0xC0) == 0x80 && "invalid UTF-8 continuation byte");
            retval <<= 6;
            retval |= ch & 0x3F;
        }
        assert(!(retval >= 0xD800 && retval < 0xE000) &&
               "invalid UTF-8 surrogate char");
        assert(retval <= 0x10FFFF && "invalid UTF-8 too big");
        if (continuation_bytes == 3)
            assert(retval >= 0x800 && "invalid UTF-8 overlong");
        else if (continuation_bytes == 4)
            assert(retval >= 0x10000 && "invalid UTF-8 overlong");
    } else {
        retval = ch;
    }
    return ParseUTF8CodepointResults{
        .ch = (char32_t)retval,
        .rest = text,
    };
}

template <std::size_t Bits, bool Signed>
struct GetBaseTypeNameInt {};

template <typename T>
struct GetBaseTypeName {};

template <std::integral T>
requires(!std::same_as<T, bool>) struct GetBaseTypeName<T>
    : public GetBaseTypeNameInt<std::numeric_limits<T>::digits +
                                    std::is_signed_v<T>,
                                std::is_signed_v<T>> {
};
} // namespace detail

#define PARSE_CHAR(T, LIMIT)                                                   \
    template <>                                                                \
    struct ParserFor<T> final {                                                \
        static ParseResult<T> parse(std::optional<TomlValue> toml,             \
                                    std::string_view field_name) noexcept {    \
            if (!toml)                                                         \
                return ParseFailure::for_missing_field(field_name);            \
            if (!toml->is_string())                                            \
                return ParseFailure::for_invalid_type(                         \
                    *toml, std::string(field_name), "character");              \
            auto s = toml->get_string();                                       \
            if (s.empty())                                                     \
                return ParseFailure::for_invalid_value(                        \
                    *toml, std::string(field_name), "character");              \
            auto results = detail::parse_utf8_codepoint(s);                    \
            if (!results.rest.empty() || (std::uint32_t)results.ch > LIMIT)    \
                return ParseFailure::for_invalid_value(                        \
                    *toml, std::string(field_name), "character");              \
            return (T)results.ch;                                              \
        }                                                                      \
    };                                                                         \
    static_assert(Parsable<T>);

PARSE_CHAR(char, 0xFFU)
PARSE_CHAR(char8_t, 0x7FU)
PARSE_CHAR(char16_t, 0xFFFFU)
PARSE_CHAR(char32_t, 0x10FFFFU)
#undef PARSE_CHAR

template <std::unsigned_integral T>
struct ParserFor<T> final {
    static ParseResult<T> parse(std::optional<TomlValue> toml,
                                std::string_view field_name) noexcept {
        if (!toml)
            return ParseFailure::for_missing_field(field_name);
        if (!toml->is_integer())
            return ParseFailure::for_invalid_type(
                *toml, std::string(field_name),
                detail::GetBaseTypeName<T>::value);
        auto value = toml->get_integer();
        T retval = static_cast<T>(value);
        auto converted_back = static_cast<decltype(value)>(retval);
        if (value < 0 || value != converted_back)
            return ParseFailure::for_invalid_value(
                *toml, std::string(field_name),
                detail::GetBaseTypeName<T>::value);
        return retval;
    }
};

template <std::signed_integral T>
struct ParserFor<T> final {
    static ParseResult<T> parse(std::optional<TomlValue> toml,
                                std::string_view field_name) noexcept {
        if (!toml)
            return ParseFailure::for_missing_field(field_name);
        if (!toml->is_integer())
            return ParseFailure::for_invalid_type(
                *toml, std::string(field_name),
                detail::GetBaseTypeName<T>::value);
        auto value = toml->get_integer();
        T retval = static_cast<T>(value);
        auto converted_back = static_cast<decltype(value)>(retval);
        if (value != converted_back)
            return ParseFailure::for_invalid_value(
                *toml, std::string(field_name),
                detail::GetBaseTypeName<T>::value);
        return retval;
    }
};

template <>
struct ParserFor<double> final {
    static ParseResult<double> parse(std::optional<TomlValue> toml,
                                     std::string_view field_name) noexcept {
        if (!toml)
            return ParseFailure::for_missing_field(field_name);
        if (toml->is_integer())
            return static_cast<double>(toml->get_integer());
        if (toml->is_float())
            return toml->get_float();
        return ParseFailure::for_invalid_type(*toml, std::string(field_name),
                                              "f64");
    }
};

template <>
struct ParserFor<float> final {
    static ParseResult<float> parse(std::optional<TomlValue> toml,
                                    std::string_view field_name) noexcept {
        if (!toml)
            return ParseFailure::for_missing_field(field_name);
        if (toml->is_integer())
            return static_cast<float>(toml->get_integer());
        if (toml->is_float())
            return static_cast<float>(toml->get_float());
        return ParseFailure::for_invalid_type(*toml, std::string(field_name),
                                              "f32");
    }
};

template <>
struct ParserFor<long double> final {
    static ParseResult<long double>
    parse(std::optional<TomlValue> toml, std::string_view field_name) noexcept {
        if (!toml)
            return ParseFailure::for_missing_field(field_name);
        if (toml->is_integer())
            return static_cast<long double>(toml->get_integer());
        if (toml->is_float())
            return static_cast<long double>(toml->get_float());
        return ParseFailure::for_invalid_type(*toml, std::string(field_name),
                                              "long double");
    }
};

namespace detail {
#define BASE_TYPE_NAME(T, N)                                                   \
    template <>                                                                \
    struct GetBaseTypeName<T> {                                                \
        static inline constexpr std::string_view value = N;                    \
    };                                                                         \
    static_assert(Parsable<T>);
#define BASE_TYPE_NAME_INT(T, bits, signed, N)                                 \
    template <>                                                                \
    struct GetBaseTypeNameInt<bits, signed> {                                  \
        static inline constexpr std::string_view value = N;                    \
    };                                                                         \
    static_assert(Parsable<T>);
BASE_TYPE_NAME(char, "char")
BASE_TYPE_NAME(char8_t, "char8_t")
BASE_TYPE_NAME(char16_t, "char16_t")
BASE_TYPE_NAME(char32_t, "char32_t")
BASE_TYPE_NAME_INT(std::uint8_t, 8, false, "u8")
BASE_TYPE_NAME_INT(std::uint16_t, 16, false, "u16")
BASE_TYPE_NAME_INT(std::uint32_t, 32, false, "u32")
BASE_TYPE_NAME_INT(std::uint64_t, 64, false, "u64")
BASE_TYPE_NAME_INT(unsigned __int128, 128, false, "u128")
BASE_TYPE_NAME_INT(std::int8_t, 8, true, "i8")
BASE_TYPE_NAME_INT(std::int16_t, 16, true, "i16")
BASE_TYPE_NAME_INT(std::int32_t, 32, true, "i32")
BASE_TYPE_NAME_INT(std::int64_t, 64, true, "i64")
BASE_TYPE_NAME_INT(__int128, 128, true, "i128")
BASE_TYPE_NAME(double, "f64")
BASE_TYPE_NAME(float, "f32")
BASE_TYPE_NAME(long double, "long double")
#undef BASE_TYPE_NAME
#undef BASE_TYPE_NAME_INT
} // namespace detail

template <Parsable T>
struct ParserFor<std::optional<T>> final {
    static ParseResult<std::optional<T>>
    parse(std::optional<TomlValue> toml, std::string_view field_name) noexcept {
        if (!toml)
            return std::nullopt;
        auto results = ParserFor<T>::parse(toml, field_name);
        if (auto *e = std::get_if<ParseFailure>(&results))
            return std::move(*e);
        return std::get<T>(std::move(results));
    }
};

static_assert(Parsable<std::optional<std::int32_t>>);

template <Parsable T>
struct ParserFor<std::vector<T>> final {
    static ParseResult<std::vector<T>>
    parse(std::optional<TomlValue> toml, std::string_view field_name) noexcept {
        if (!toml)
            return ParseFailure::for_missing_field(field_name);
        if (!toml->is_array())
            return ParseFailure::for_invalid_type(
                *toml, std::string(field_name), "sequence");
        auto toml_array = toml->get_array();
        std::vector<T> retval;
        retval.reserve(toml_array.size());
        for (TomlValue toml : toml_array) {
            auto results = ParserFor<T>::parse(toml, field_name);
            if (auto *e = std::get_if<ParseFailure>(&results))
                return std::move(*e);
            retval.push_back(std::get<T>(std::move(results)));
        }
        return std::move(retval);
    }
};

static_assert(Parsable<std::vector<std::int32_t>>);

template <std::copyable T>
ParseResult<T>
parse_enum(std::optional<TomlValue> toml, std::string_view field_name,
           std::span<const std::pair<T, std::string_view>> enumerants) {
    if (!toml)
        return ParseFailure::for_missing_field(field_name);
    if (!toml->is_string())
        return ParseFailure::for_invalid_type(*toml, std::string(field_name),
                                              "string");
    auto string = toml->get_string();
    for (auto &[value, name] : enumerants) {
        if (string == name)
            return value;
    }
    return ParseFailure::for_unknown_variant(string, std::string(field_name),
                                             enumerants);
}

template <std::copyable T>
ParseResult<T>
parse_enum(std::optional<TomlValue> toml, std::string_view field_name,
           std::initializer_list<std::pair<T, std::string_view>> enumerants) {
    return parse_enum(
        toml, field_name,
        std::span<const std::pair<T, std::string_view>>(enumerants));
}

namespace detail {
template <ParsableStruct T>
using FieldParseFn = std::function<std::optional<ParseFailure>(
    T *struct_, std::optional<TomlValue> toml)>;

constexpr inline std::string_view NAME_FIELD_NAME = "name";

template <ParsableStruct T, Parsable F>
bool make_field_parse_functions_helper(
    std::map<std::string_view, FieldParseFn<T>> &retval,
    const Field<T, F> field) {
    assert(field.name != NAME_FIELD_NAME &&
           "can't have a struct field named `name`");
    auto parse_field = FieldParseFn<T>(
        [field](T *struct_,
                std::optional<TomlValue> toml) -> std::optional<ParseFailure> {
            ParseResult<F> results = ParserFor<F>::parse(toml, field.name);
            if (ParseFailure *e = std::get_if<ParseFailure>(&results))
                return std::move(*e);
            struct_->*(field.field) = std::get<F>(std::move(results));
            return std::nullopt;
        });
    return retval.insert({field.name, parse_field}).second;
}

template <ParsableStruct T, Parsable... Fields>
std::map<std::string_view, FieldParseFn<T>>
make_field_parse_functions_helper(Field<T, Fields>... fields) {
    std::map<std::string_view, FieldParseFn<T>> retval;
    bool no_duplicates =
        (true && ... && make_field_parse_functions_helper(retval, fields));
    assert(no_duplicates);
    return retval;
}

template <ParsableStruct T, Parsable... Fields>
std::map<std::string_view, FieldParseFn<T>>
make_field_parse_functions(std::tuple<Field<T, Fields>...> fields) {
    return std::apply(
        static_cast<std::map<std::string_view, FieldParseFn<T>> (*)(
            Field<T, Fields>... fields)>(make_field_parse_functions_helper),
        fields);
}

template <ParsableStruct T>
const std::map<std::string_view, FieldParseFn<T>> &get_field_parse_functions() {
    static const std::map<std::string_view, FieldParseFn<T>> retval =
        make_field_parse_functions<T>(T::FIELDS);
    return retval;
}

template <ParsableStruct... Structs>
using VariantOfStructsParseFunction =
    std::function<ParseResult<std::variant<Structs...>>(
        TomlTable toml_table, std::string_view parent_field_name)>;

template <ParsableStruct T, ParsableStruct... Structs>
ParseResult<std::variant<Structs...>>
variant_of_structs_parse_function(TomlTable toml_table,
                                  std::string_view parent_field_name) {
    const std::map<std::string_view, FieldParseFn<T>> &field_parse_functions =
        get_field_parse_functions<T>();
    for (auto field_name : toml_table.keys()) {
        if (field_parse_functions.contains(field_name))
            continue;
        if (field_name != NAME_FIELD_NAME) {
            std::vector<std::string_view> field_names;
            field_names.reserve(field_parse_functions.size());
            for (auto &[field_name, _] : field_parse_functions) {
                field_names.push_back(field_name);
            }
            return ParseFailure::for_unknown_field(field_name, field_names,
                                                   parent_field_name);
        }
        auto results = parse_enum<int>(toml_table.get_opt(field_name),
                                       field_name, {{0, T::NAME}});
        if (ParseFailure *e = std::get_if<ParseFailure>(&results)) {
            e->insert_parent_field_name(parent_field_name);
            return std::move(*e);
        }
    }
    T retval{};
    for (auto &[field_name, field_parse_fn] : field_parse_functions) {
        auto parse_failure = field_parse_fn(std::addressof(retval),
                                            toml_table.get_opt(field_name));
        if (parse_failure) {
            parse_failure->insert_parent_field_name(parent_field_name);
            return std::move(*parse_failure);
        }
    }
    return std::move(retval);
}

template <ParsableStruct... Structs>
using VariantOfStructsParseFunctionsMap =
    std::map<std::string_view, VariantOfStructsParseFunction<Structs...>>;

template <ParsableStruct... Structs>
VariantOfStructsParseFunctionsMap<Structs...>
make_variant_of_structs_parse_functions() {
    VariantOfStructsParseFunctionsMap<Structs...> retval;
    bool no_duplicates =
        (true && ... &&
         retval
             .insert({Structs::NAME,
                      variant_of_structs_parse_function<Structs, Structs...>})
             .second);
    assert(no_duplicates);
    return retval;
}

template <ParsableStruct... Structs>
const VariantOfStructsParseFunctionsMap<Structs...> &
get_variant_of_structs_parse_functions() {
    static const VariantOfStructsParseFunctionsMap<Structs...> retval =
        make_variant_of_structs_parse_functions<Structs...>();
    return retval;
}

template <ParsableStruct... Structs>
ParseResult<std::variant<Structs...>>
parse_variant_of_structs(std::optional<TomlValue> toml,
                         std::string_view parent_field_name,
                         std::string_view overall_type_name = "map") {
    if (!toml)
        return ParseFailure::for_missing_field(parent_field_name);
    if (!toml->is_table())
        return ParseFailure::for_invalid_type(
            *toml, std::string(parent_field_name), overall_type_name);
    auto toml_table = toml->get_table();
    const auto &parse_functions =
        get_variant_of_structs_parse_functions<Structs...>();
    auto name_field = toml_table.get_opt(NAME_FIELD_NAME);
    if (!name_field) {
        if (parse_functions.size() != 1)
            return ParseFailure::for_missing_field(
                NAME_FIELD_NAME, std::string(parent_field_name));
        return parse_functions.begin()->second(toml_table, parent_field_name);
    }
    if (!name_field->is_string()) {
        auto retval = ParseFailure::for_invalid_type(
            *name_field, std::string(NAME_FIELD_NAME), "string");
        retval.insert_parent_field_name(parent_field_name);
        return retval;
    }
    auto iter = parse_functions.find(name_field->get_string());
    if (iter != parse_functions.end())
        return iter->second(toml_table, parent_field_name);
    std::vector<std::pair<int, std::string_view>> enumerants;
    enumerants.reserve(parse_functions.size());
    for (auto &[name, _] : parse_functions) {
        enumerants.push_back({0, name});
    }
    auto retval = ParseFailure::for_unknown_variant<int>(
        name_field->get_string(), std::string(NAME_FIELD_NAME), enumerants);
    retval.insert_parent_field_name(parent_field_name);
    return retval;
}

template <ParsableStruct T>
struct DeduceParserFor<T, void> {
    static ParseResult<T> parse(std::optional<TomlValue> toml,
                                std::string_view field_name) {
        auto results = parse_variant_of_structs<T>(toml, field_name);
        if (ParseFailure *e = std::get_if<ParseFailure>(&results))
            return std::move(*e);
        return std::get<T>(std::get<std::variant<T>>(std::move(results)));
    }
};
} // namespace detail

template <detail::ParsableStruct... Structs>
struct ParserFor<std::variant<Structs...>> final {
    static ParseResult<std::variant<Structs...>>
    parse(std::optional<TomlValue> toml, std::string_view field_name) {
        return detail::parse_variant_of_structs<Structs...>(toml, field_name);
    }
};

template <Parsable T>
T parse_or_exit(TomlValue toml) {
    ParseResult<T> results = ParserFor<T>::parse(toml, "");
    if (ParseFailure *e = std::get_if<ParseFailure>(&results))
        e->print_and_exit();
    return std::get<T>(std::move(results));
}

namespace detail {
enum class TestEnum1 {
    A,
    B,
    C,
};
}

template <>
struct ParserFor<detail::TestEnum1> final {
    static ParseResult<detail::TestEnum1> parse(std::optional<TomlValue> toml,
                                                std::string_view field_name) {
        using E = detail::TestEnum1;
        return parse_enum<E>(toml, field_name,
                             {
                                 {E::A, "A"},
                                 {E::B, "B"},
                                 {E::C, "C"},
                             });
    }
};

namespace detail {
struct HasFieldsConstTest1 final {
    static constexpr inline std::tuple<> FIELDS = {};
};

static_assert(HasFieldsConst<HasFieldsConstTest1>);

struct HasFieldsConstTest2 final {
    int field1, field2;
    char field3;
    static constexpr inline std::tuple FIELDS = {
        Field{&HasFieldsConstTest2::field1, "field1"},
        Field{&HasFieldsConstTest2::field2, "field2"},
        Field{&HasFieldsConstTest2::field3, "field3"},
    };
};

static_assert(HasFieldsConst<HasFieldsConstTest2>);

struct HasFieldsConstTestWrongClass final {
    static constexpr inline std::tuple FIELDS = {
        Field{&HasFieldsConstTest2::field1, "field1"},
    };
};

static_assert(!HasFieldsConst<HasFieldsConstTestWrongClass>);

struct HasFieldsConstTestNoFieldsConst final {};

static_assert(!HasFieldsConst<HasFieldsConstTestNoFieldsConst>);

struct ParsableStructTest1 final {
    static constexpr inline std::tuple FIELDS = {};
    static constexpr inline std::string_view NAME = "ParsableStructTest1";
};

static_assert(ParsableStruct<ParsableStructTest1>);
static_assert(Parsable<ParsableStructTest1>);

struct ParsableStructTest2 final {
    int field1;
    static constexpr inline std::tuple FIELDS = {
        Field{&ParsableStructTest2::field1, "field1"},
    };
    static constexpr inline std::string_view NAME = "ParsableStructTest2";
};

static_assert(ParsableStruct<ParsableStructTest2>);
static_assert(Parsable<ParsableStructTest2>);
static_assert(Parsable<std::variant<ParsableStructTest1, ParsableStructTest2>>);

} // namespace detail

template <std::integral T>
requires(!std::same_as<T, bool>) //
    struct NonZero final {
    T value;
    constexpr NonZero() noexcept : value(1) {}
    constexpr explicit NonZero(T value) noexcept : value(value) {
        assert(value != 0);
    }
    static constexpr std::optional<NonZero<T>> from(T value) noexcept {
        if (value == 0)
            return std::nullopt;
        return NonZero(value);
    }
    constexpr operator T() const noexcept {
        return value;
    }
};

template <std::integral T>
requires Parsable<T> &&(!std::same_as<T, bool>) //
    struct ParserFor<NonZero<T>> final {
    static ParseResult<NonZero<T>> parse(std::optional<TomlValue> toml,
                                         std::string_view field_name) {
        ParseResult<T> results = ParserFor<T>::parse(toml, field_name);
        if (ParseFailure *e = std::get_if<ParseFailure>(&results))
            return std::move(*e);
        T value = std::get<T>(results);
        if (value == 0)
            return ParseFailure::for_expected_nonzero_value(
                std::string(field_name));
        return NonZero<T>(value);
    }
};

static_assert(Parsable<NonZero<std::int32_t>>);

namespace detail {
template <Parsable K, Parsable V, std::movable Retval>
requires std::default_initializable<Retval> &&
    requires(Retval &retval, K k, V v) {
    retval.insert({std::move(k), std::move(v)});
}
ParseResult<Retval> parse_map(std::optional<TomlValue> toml,
                              std::string_view parent_field_name) {
    if (!toml)
        return ParseFailure::for_missing_field(parent_field_name);
    if (!toml->is_table())
        return ParseFailure::for_invalid_type(
            *toml, std::string(parent_field_name), "map");
    Retval retval;
    for (auto &[field_name, value_toml] : toml->get_table()) {
        ParseResult<K> key_result =
            ParserFor<K>::parse(TomlValue(field_name), parent_field_name);
        if (ParseFailure *e = std::get_if<ParseFailure>(&key_result))
            return std::move(*e);
        ParseResult<V> value_result =
            ParserFor<V>::parse(value_toml, field_name);
        if (ParseFailure *e = std::get_if<ParseFailure>(&value_result))
            return std::move(*e);
        retval.insert({std::get<K>(std::move(key_result)),
                       std::get<V>(std::move(value_result))});
    }
    return retval;
}
} // namespace detail

template <Parsable K, Parsable V, typename Hash, typename Eq>
struct ParserFor<std::unordered_map<K, V, Hash, Eq>> final {
    static ParseResult<std::unordered_map<K, V, Hash, Eq>>
    parse(std::optional<TomlValue> toml, std::string_view field_name) {
        return detail::parse_map<K, V, std::unordered_map<K, V, Hash, Eq>>(
            toml, field_name);
    }
};

static_assert(Parsable<std::unordered_map<std::string, std::int32_t>>);

template <Parsable K, Parsable V, typename Compare>
struct ParserFor<std::map<K, V, Compare>> final {
    static ParseResult<std::map<K, V, Compare>>
    parse(std::optional<TomlValue> toml, std::string_view field_name) {
        return detail::parse_map<K, V, std::map<K, V, Compare>>(toml,
                                                                field_name);
    }
};

static_assert(Parsable<std::map<std::string, std::int32_t>>);

template <Parsable K, Parsable V, typename Hash, typename Eq>
struct ParserFor<std::unordered_multimap<K, V, Hash, Eq>> final {
    static ParseResult<std::unordered_multimap<K, V, Hash, Eq>>
    parse(std::optional<TomlValue> toml, std::string_view field_name) {
        return detail::parse_map<K, V, std::unordered_multimap<K, V, Hash, Eq>>(
            toml, field_name);
    }
};

static_assert(Parsable<std::unordered_multimap<std::string, std::int32_t>>);

template <Parsable K, Parsable V, typename Compare>
struct ParserFor<std::multimap<K, V, Compare>> final {
    static ParseResult<std::multimap<K, V, Compare>>
    parse(std::optional<TomlValue> toml, std::string_view field_name) {
        return detail::parse_map<K, V, std::multimap<K, V, Compare>>(
            toml, field_name);
    }
};

static_assert(Parsable<std::multimap<std::string, std::int32_t>>);

namespace detail {
template <Parsable K, std::movable Retval>
requires std::default_initializable<Retval> && requires(Retval &retval, K k) {
    retval.insert(std::move(k));
}
ParseResult<Retval> parse_set(std::optional<TomlValue> toml,
                              std::string_view parent_field_name) {
    if (!toml)
        return ParseFailure::for_missing_field(parent_field_name);
    if (!toml->is_array())
        return ParseFailure::for_invalid_type(
            *toml, std::string(parent_field_name), "sequence");
    Retval retval;
    for (auto key_toml : toml->get_array()) {
        ParseResult<K> key_result =
            ParserFor<K>::parse(key_toml, parent_field_name);
        if (ParseFailure *e = std::get_if<ParseFailure>(&key_result))
            return std::move(*e);
        retval.insert(std::get<K>(std::move(key_result)));
    }
    return retval;
}
} // namespace detail

template <Parsable K, typename Hash, typename Eq>
struct ParserFor<std::unordered_set<K, Hash, Eq>> final {
    static ParseResult<std::unordered_set<K, Hash, Eq>>
    parse(std::optional<TomlValue> toml, std::string_view field_name) {
        return detail::parse_set<K, std::unordered_set<K, Hash, Eq>>(
            toml, field_name);
    }
};

static_assert(Parsable<std::unordered_set<std::string>>);

template <Parsable K, typename Compare>
struct ParserFor<std::set<K, Compare>> final {
    static ParseResult<std::set<K, Compare>>
    parse(std::optional<TomlValue> toml, std::string_view field_name) {
        return detail::parse_set<K, std::set<K, Compare>>(toml, field_name);
    }
};

static_assert(Parsable<std::set<std::string>>);

template <Parsable K, typename Hash, typename Eq>
struct ParserFor<std::unordered_multiset<K, Hash, Eq>> final {
    static ParseResult<std::unordered_multiset<K, Hash, Eq>>
    parse(std::optional<TomlValue> toml, std::string_view field_name) {
        return detail::parse_set<K, std::unordered_multiset<K, Hash, Eq>>(
            toml, field_name);
    }
};

static_assert(Parsable<std::unordered_multiset<std::string>>);

template <Parsable K, typename Compare>
struct ParserFor<std::multiset<K, Compare>> final {
    static ParseResult<std::multiset<K, Compare>>
    parse(std::optional<TomlValue> toml, std::string_view field_name) {
        return detail::parse_set<K, std::multiset<K, Compare>>(toml,
                                                               field_name);
    }
};

static_assert(Parsable<std::multiset<std::string>>);

} // namespace config_parser