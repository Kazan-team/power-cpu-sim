// SPDX-License-Identifier: LGPL-2.1-or-later
// See Notices.txt for copyright information
#pragma once

#include "arc.h"
#include <algorithm>
#include <cassert>
#include <compare>
#include <concepts>
#include <cstdint>
#include <deque>
#include <memory>
#include <optional>
#include <queue>
#include <tuple>
#include <type_traits>
#include <unordered_map>
#include <unordered_set>
#include <vector>

namespace simulator {
struct SimulationTime final {
    using value_t = std::uint64_t;
    value_t value;
    constexpr explicit SimulationTime(std::uint64_t value = 0) noexcept
        : value(value) {}
    constexpr auto operator<=>(const SimulationTime &) const noexcept = default;
    constexpr value_t lsb() const noexcept {
        return value & 1;
    }
    constexpr SimulationTime next() const noexcept {
        return SimulationTime(value + 1);
    }
};

enum class StateChanged : bool {
    Unchanged = false,
    Changed = true,
};

constexpr StateChanged operator|(StateChanged a, StateChanged b) noexcept {
    if (a == StateChanged::Changed) {
        return a;
    }
    return b;
}

constexpr StateChanged &operator|=(StateChanged &a, StateChanged b) noexcept {
    a = a | b;
    return a;
}

struct GlobalState final {
    SimulationTime current_time{};
    StateChanged state_changed = StateChanged::Unchanged;
};

template <std::regular T>
class State;

class Simulator;

class StateBase {
    template <std::regular T>
    friend class State;

private:
    Arc<GlobalState> m_global_state;
    explicit StateBase(Simulator &simulator) noexcept;
    StateBase(const StateBase &) = delete;
    StateBase &operator=(const StateBase &) = delete;

public:
    StateBase(StateBase &&) = default;
    StateBase &operator=(StateBase &&) = default;
    virtual ~StateBase() = default;
};

template <std::regular T>
class State final : public StateBase {
private:
    // m_last_updated_time is used for tracking which element of m_states is
    // current and which is next, not for tracking the last time this state
    // changed
    SimulationTime m_last_updated_time;
    T m_states[2];

public:
    State(Simulator &simulator, T state = T())
        : StateBase(simulator),
          m_last_updated_time(), m_states{state, std::move(state)} {}
    const T &current() const noexcept {
        if (m_last_updated_time < m_global_state->current_time)
            return m_states[m_last_updated_time.lsb() ^ 1];
        return m_states[m_last_updated_time.lsb()];
    }
    const T &operator*() const noexcept {
        return current();
    }
    const T *operator->() const noexcept {
        return std::addressof(current());
    }

private:
    T &next_ref() noexcept {
        if (m_last_updated_time < m_global_state->current_time)
            return m_states[m_last_updated_time.lsb()];
        return m_states[m_last_updated_time.lsb() ^ 1];
    }

public:
    StateChanged set_next(T value) noexcept {
        auto current_time = m_global_state->current_time;
        if (m_last_updated_time < current_time) {
            next_ref() = current();
            m_last_updated_time = current_time;
        }
        auto &next = next_ref();
        if (next != value) {
            next = value;
            m_global_state->state_changed = StateChanged::Changed;
            return StateChanged::Changed;
        }
        return StateChanged::Unchanged;
    }
};

class Simulatable {
public:
    constexpr Simulatable() noexcept = default;
    virtual StateChanged compute_next_state(Simulator &simulator) noexcept = 0;
    virtual void on_step_before_settle(Simulator &simulator) noexcept {
        (void)simulator;
    }
    virtual void on_settle(Simulator &simulator) noexcept {
        (void)simulator;
    }
    virtual ~Simulatable() = default;
};

class Simulator final {
    friend class StateBase;

private:
    Arc<GlobalState> m_global_state;

    /// only use for_each_simulatable to iterate through
    std::vector<Arc<Simulatable>> m_simulatables;
    std::unordered_set<Simulatable *> m_simulatables_set;

private:
    /// iterate through `m_simulatables` in a memory-safe way allowing new
    /// `Simulatable`s to be inserted during iteration
    template <std::invocable<Simulatable *> F>
    void for_each_simulatable(F &&f) {
        for (std::size_t i = 0; i < m_simulatables.size(); i++) {
            // can't use range-for loop since m_simulatables may expand
            // while iterating
            Simulatable &simulatable = *m_simulatables[i];
            std::invoke(std::forward<F>(f), &simulatable);
        }
    }

public:
    Simulator() noexcept
        : m_global_state(make_arc<GlobalState>()), m_simulatables(),
          m_simulatables_set() {}
    Simulator(const Simulator &) = delete;
    Simulator &operator=(const Simulator &) = delete;
    void inputs_changed() noexcept {
        m_global_state->state_changed = StateChanged::Changed;
    }
    StateChanged settle() noexcept {
        if (m_global_state->state_changed == StateChanged::Unchanged) {
            return StateChanged::Unchanged;
        }
        std::size_t loop_count = 0;
        while (m_global_state->state_changed == StateChanged::Changed) {
            assert(++loop_count < 10);
            m_global_state->state_changed = StateChanged::Unchanged;
            for_each_simulatable([&](Simulatable *simulatable) {
                simulatable->compute_next_state(*this);
            });
        }
        for_each_simulatable(
            [&](Simulatable *simulatable) { simulatable->on_settle(*this); });
        return StateChanged::Changed;
    }
    SimulationTime current_time() const noexcept {
        return m_global_state->current_time;
    }
    void step() noexcept {
        settle();
        m_global_state->current_time = m_global_state->current_time.next();
        m_global_state->state_changed = StateChanged::Changed;
        for_each_simulatable([&](Simulatable *simulatable) {
            simulatable->on_step_before_settle(*this);
        });
        settle();
    }
    /// insert a new `Simulatable` into the simulation.
    /// Can be called at any time, including during execution of `Simulatable`
    /// member functions.
    bool insert(Arc<Simulatable> simulatable) noexcept {
        auto [_, inserted] =
            m_simulatables_set.insert(std::addressof(*simulatable));
        if (inserted) {
            m_global_state->state_changed = StateChanged::Changed;
            m_simulatables.push_back(simulatable);
        }
        return inserted;
    }
};

inline StateBase::StateBase(Simulator &simulator) noexcept
    : m_global_state(simulator.m_global_state) {}
} // namespace simulator