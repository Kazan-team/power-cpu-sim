// SPDX-License-Identifier: LGPL-2.1-or-later
// See Notices.txt for copyright information
#pragma once

#include "config.h"
#include "instruction_state.h"
#include "pipe.h"
#include "simulator.h"
#include <deque>
#include <memory>
#include <unordered_set>
#include <vector>

class RetireInputPipe final : public Pipe {
public:
    using Pipe::Pipe;
    simulator::StateChanged
    compute_next_state(simulator::Simulator &) noexcept final {
        return simulator::StateChanged::Unchanged;
    }
    PipeInstructionsRef pipe_output_instructions() const noexcept final {
        return pipe_input_instructions();
    }
    static Arc<const PipeFactory> make_factory() {
        return PipeFactory::make_factory_for<RetireInputPipe, Instruction>();
    }
};

class RetirePipe final : public Pipe {
private:
    struct State final {
        std::vector<std::shared_ptr<InstructionState>> output_instructions;
        std::unordered_set<std::shared_ptr<InstructionState>>
            waiting_instructions;
        std::deque<std::shared_ptr<InstructionState>> instruction_queue;
        bool operator==(const State &) const noexcept = default;
    };
    simulator::State<State> m_state;
    Arc<RetireInputPipe> m_retire_input_pipe;

public:
    RetirePipe(Arc<Pipe> input_pipe, Arc<const PipeFactory> factory,
               simulator::Simulator &simulator,
               Arc<RetireInputPipe> retire_input_pipe,
               const config::RetirePipe &cfg) noexcept
        : Pipe(std::move(input_pipe), std::move(factory), simulator),
          m_state(simulator, {.output_instructions{},
                              .waiting_instructions{},
                              .instruction_queue{}}),
          m_retire_input_pipe(std::move(retire_input_pipe)) {
        (void)cfg;
    }
    simulator::StateChanged
    compute_next_state(simulator::Simulator &) noexcept final {
        auto changed = simulator::StateChanged::Unchanged;
        auto state = m_state.current();
        {
            auto input_instructions =
                m_retire_input_pipe->pipe_input_instructions();
            for (auto &instruction : input_instructions.instructions) {
                state.instruction_queue.push_back(instruction);
            }
        }
        {
            auto input_instructions = pipe_input_instructions();
            for (auto &instruction : input_instructions.instructions) {
                state.waiting_instructions.insert(instruction);
            }
        }
        state.output_instructions.clear();
        while (!state.instruction_queue.empty()) {
            if (state.waiting_instructions.erase(
                    state.instruction_queue.front()) == 0)
                break;
            auto instruction = state.instruction_queue.front();
            state.instruction_queue.pop_front();
            changed |= instruction->state.set_next("Retire");
            state.output_instructions.push_back(std::move(instruction));
        }
        for (auto &instruction : state.waiting_instructions) {
            changed |= instruction->state.set_next("Wait: Retire");
        }
        changed |= m_state.set_next(std::move(state));
        return changed;
    }
    PipeInstructionsRef pipe_output_instructions() const override {
        return {.instructions =
                    std::span(m_state.current().output_instructions),
                .pipe_ref = arc_from_this()};
    }
    static Arc<const PipeFactory>
    make_factory(Arc<RetireInputPipe> retire_input_pipe,
                 const config::RetirePipe &cfg) {
        return PipeFactory::make_factory_for<RetirePipe, Instruction>(
            std::move(retire_input_pipe), cfg);
    }
};
