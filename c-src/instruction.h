// SPDX-License-Identifier: LGPL-2.1-or-later
// See Notices.txt for copyright information
#pragma once

#include "arc.h"
#include "config_parser.h"
#include "register.h"
#include <array>
#include <concepts>
#include <cstdint>
#include <cstdlib>
#include <initializer_list>
#include <memory>
#include <ostream>
#include <span>
#include <sstream>
#include <string_view>
#include <utility>

class Instruction {
public:
    enum class Kind : std::size_t;
    friend struct config_parser::ParserFor<Instruction::Kind>;

private:
    static const std::initializer_list<std::pair<Kind, std::string_view>>
        kind_names;

public:
    static constexpr std::string_view name(Kind kind) noexcept;
    friend std::ostream &operator<<(std::ostream &os, Kind kind) {
        os << name(kind);
        return os;
    }
    constexpr virtual ~Instruction() = default;
    constexpr virtual std::span<Register> inputs() noexcept = 0;
    constexpr virtual std::span<Register> outputs() noexcept = 0;
    constexpr virtual std::span<const Register> inputs() const noexcept = 0;
    constexpr virtual std::span<const Register> outputs() const noexcept = 0;
    constexpr virtual Kind kind() const noexcept = 0;
    constexpr std::string_view name() const noexcept {
        return name(kind());
    }
    constexpr virtual bool is_equal(const Instruction &rhs) const noexcept = 0;
    constexpr bool operator==(const Instruction &rhs) const noexcept {
        return is_equal(rhs);
    }
    constexpr virtual std::uint64_t size_in_bytes() const noexcept {
        return 4;
    }
    virtual Arc<Instruction> clone() const = 0;
    template <typename Immediate = const std::string_view,
              std::size_t N = std::dynamic_extent>
    requires requires(std::ostream &os, Immediate &imm) {
        (void)(os << imm);
    }
    static void write_impl(std::ostream &os, std::string_view name,
                           std::span<const Register> outputs,
                           std::span<const Register> inputs,
                           std::span<Immediate, N> immediates = {}) {
        os << name;
        auto write_things = [&](auto things,
                                std::string_view initial_separator) {
            std::string_view separator = initial_separator;
            for (auto &thing : things) {
                os << separator;
                (void)(os << thing);
                separator = ", ";
            }
        };
        if (inputs.empty()) {
            if (outputs.empty()) {
                write_things(immediates, " ");
                return;
            } else {
                write_things(outputs, " -> ");
            }
        } else if (outputs.empty()) {
            write_things(inputs, " ");
        } else {
            write_things(outputs, " ");
            write_things(inputs, " <- ");
        }
        write_things(immediates, ", ");
    }
    virtual void write(std::ostream &os) const {
        write_impl(os, name(), outputs(), inputs());
    }
    friend std::ostream &operator<<(std::ostream &os, const Instruction &i) {
        i.write(os);
        return os;
    }
    std::string to_string() const {
        std::ostringstream ss;
        ss << *this;
        return ss.str();
    }
};

template <>
struct config_parser::ParserFor<Instruction::Kind> final {
    static ParseResult<Instruction::Kind> parse(std::optional<TomlValue> toml,
                                                std::string_view field_name) {
        return config_parser::parse_enum(toml, field_name,
                                         Instruction::kind_names);
    }
};

static_assert(config_parser::Parsable<Instruction::Kind>);

template <std::size_t OutputCount, std::size_t InputCount>
class InstructionHelper : public Instruction {
private:
    std::array<Register, OutputCount> m_outputs;
    std::array<Register, InputCount> m_inputs;

public:
    constexpr InstructionHelper(
        std::array<Register, OutputCount> outputs,
        std::array<Register, InputCount> inputs) noexcept
        : m_outputs(std::move(outputs)), m_inputs(std::move(inputs)) {}
    constexpr std::span<Register> inputs() noexcept final {
        return m_inputs;
    }
    constexpr std::span<Register> outputs() noexcept final {
        return m_outputs;
    }
    constexpr std::span<const Register> inputs() const noexcept final {
        return m_inputs;
    }
    constexpr std::span<const Register> outputs() const noexcept final {
        return m_outputs;
    }
};

/// an Instruction type that can't be instantiated -- useful to denote a Pipe
/// that can't handle any input instructions
class NoInstruction final : public Instruction {
public:
    NoInstruction() = delete;
    // still copy/move constructible/assignable to allow code to compile
    constexpr std::span<Register> inputs() noexcept final {
        return {};
    }
    constexpr std::span<Register> outputs() noexcept final {
        return {};
    }
    constexpr std::span<const Register> inputs() const noexcept final {
        return {};
    }
    constexpr std::span<const Register> outputs() const noexcept final {
        return {};
    }
    constexpr Kind kind() const noexcept final {
        return {};
    }
    constexpr bool is_equal(const Instruction &) const noexcept final {
        return false;
    }
    Arc<Instruction> clone() const noexcept final {
        assert(!"can't be constructed");
        std::abort();
    }
};
