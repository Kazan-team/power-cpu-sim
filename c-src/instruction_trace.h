// SPDX-License-Identifier: LGPL-2.1-or-later
// See Notices.txt for copyright information
#pragma once

#include "instruction.h"
#include "instruction_state.h"
#include "simulator.h"
#include "tracking_table_writer.h"
#include <concepts>
#include <memory>
#include <span>
#include <sstream>
#include <utility>
#include <vector>

class InstructionTrace final : public simulator::Simulatable {
private:
    std::vector<std::shared_ptr<InstructionState>> m_instructions;

public:
    explicit InstructionTrace(std::vector<std::shared_ptr<InstructionState>>
                                  instruction_sequence) noexcept
        : m_instructions(std::move(instruction_sequence)) {}
    std::span<const std::shared_ptr<InstructionState>>
    instructions() const noexcept {
        return m_instructions;
    }
    static Arc<const InstructionTrace>
    make(std::span<const std::pair<InstructionAddress, Arc<Instruction>>>
             instructions,
         simulator::Simulator &simulator,
         TrackingTableWriterBuilder &table_writer_builder) {
        std::vector<std::shared_ptr<InstructionState>> instruction_sequence;
        instruction_sequence.reserve(instructions.size());
        for (auto &[address, instruction] : instructions) {
            simulator::State<std::string> state(simulator, "");
            std::ostringstream ss;
            ss << "0x" << std::hex << address << std::dec << ": "
               << *instruction;
            auto table_column = table_writer_builder.add_column(ss.str());
            simulator::State<Arc<Instruction>> instruction_state(simulator,
                                                                 instruction);
            instruction_sequence.push_back(std::make_shared<InstructionState>(
                InstructionState{.address = address,
                                 .instruction = std::move(instruction_state),
                                 .state = std::move(state),
                                 .table_column = std::move(table_column)}));
        }
        auto retval =
            make_arc<InstructionTrace>(std::move(instruction_sequence));
        simulator.insert(retval);
        return retval;
    }
    class Builder final {
    private:
        std::vector<std::pair<InstructionAddress, Arc<Instruction>>>
            m_instructions;

    public:
        Builder() noexcept : m_instructions() {}
        template <std::derived_from<Instruction> T>
        requires std::movable<T> //
        void add(InstructionAddress address, T instruction) {
            m_instructions.push_back(
                {address, make_arc<T>(std::move(instruction))});
        }
        template <std::derived_from<Instruction> T>
        void add(InstructionAddress address, Arc<T> instruction) {
            m_instructions.push_back({address, std::move(instruction)});
        }

        Arc<const InstructionTrace>
        build(simulator::Simulator &simulator,
              TrackingTableWriterBuilder &table_writer_builder) const {
            return InstructionTrace::make(m_instructions, simulator,
                                          table_writer_builder);
        }
    };
    simulator::StateChanged
    compute_next_state(simulator::Simulator &) noexcept final {
        return simulator::StateChanged::Unchanged;
    }
    void update_table_columns() {
        for (auto &instruction : instructions()) {
            instruction->table_column->set_value(instruction->state.current());
        }
    }
    void on_settle(simulator::Simulator &) noexcept final {
        update_table_columns();
    }
    void on_step_before_settle(simulator::Simulator &) noexcept final {
        update_table_columns();
    }
};
