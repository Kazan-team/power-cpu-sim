// SPDX-License-Identifier: LGPL-2.1-or-later
// See Notices.txt for copyright information
#pragma once

#include <cassert>
#include <compare>
#include <concepts>
#include <cstddef>
#include <memory>
#include <type_traits>

// an atomically reference-counted pointer, except equality is defined based on
// equality of the contained type, not pointer equality. Like Rust's
// `Option<Arc<T>>`.
template <typename T>
class Arc final {
    template <typename U>
    friend class Arc;

private:
    std::shared_ptr<T> m_value;

public:
    Arc() noexcept : m_value() {}
    Arc(const Arc &) noexcept = default;
    Arc(Arc &&) noexcept = default;
    Arc &operator=(const Arc &) noexcept = default;
    Arc &operator=(Arc &&) noexcept = default;
    Arc(std::nullptr_t) noexcept : m_value() {}
    explicit Arc(std::shared_ptr<T> value) noexcept
        : m_value(std::move(value)) {}
    template <typename U>
    requires requires(const std::shared_ptr<U> &v) {
        std::shared_ptr<T>(v);
    }
    Arc(const Arc<U> &rhs) noexcept : m_value(rhs.m_value) {}
    template <typename U>
    requires requires(std::shared_ptr<U> &v) {
        std::shared_ptr<T>(std::move(v));
    }
    Arc(Arc<U> &&rhs) noexcept : m_value(std::move(rhs.m_value)) {}
    template <typename U>
    requires requires(std::shared_ptr<T> &l, const std::shared_ptr<U> &v) {
        l = v;
    }
    Arc &operator=(const Arc<U> &rhs) noexcept {
        m_value = rhs.m_value;
        return *this;
    }
    template <typename U>
    requires requires(std::shared_ptr<T> &l, std::shared_ptr<U> &v) {
        l = std::move(v);
    }
    Arc &operator=(Arc<U> &&rhs) noexcept {
        m_value = std::move(rhs.m_value);
        return *this;
    }
    explicit operator bool() const noexcept {
        return static_cast<bool>(m_value);
    }
    bool operator!() const noexcept {
        return !m_value;
    }
    T *operator->() const noexcept {
        return m_value.operator->();
    }
    std::add_lvalue_reference_t<T> operator*() const noexcept {
        assert(m_value);
        return *m_value;
    }
    explicit operator const std::shared_ptr<T> &() const noexcept {
        return m_value;
    }
    explicit operator std::shared_ptr<T> &&() &&noexcept {
        return std::move(m_value);
    }
    void swap(Arc &other) noexcept {
        m_value.swap(other.m_value);
    }
};

template <typename T>
inline bool operator==(const Arc<T> &t, std::nullptr_t) noexcept {
    return !t;
}

template <typename T>
inline std::strong_ordering operator<=>(const Arc<T> &t,
                                        std::nullptr_t) noexcept {
    if (t)
        return std::strong_ordering::greater;
    return std::strong_ordering::equal;
}

template <typename T>
inline bool operator==(std::nullptr_t, const Arc<T> &t) noexcept {
    return !t;
}

template <typename T>
inline std::strong_ordering operator<=>(std::nullptr_t,
                                        const Arc<T> &t) noexcept {
    if (t)
        return std::strong_ordering::less;
    return std::strong_ordering::equal;
}

template <typename T, std::equality_comparable_with<T> U>
inline bool operator==(const Arc<T> &t, const Arc<U> &u) noexcept(
    noexcept(std::declval<const T &>() == std::declval<const U &>() ? 0 : 0)) {
    if (!t)
        return !u;
    if (!u)
        return false;
    const T &t_v = *t;
    const U &u_v = *u;
    if (t_v == u_v)
        return true;
    return false;
}

template <class T, std::totally_ordered_with<T> U>
requires requires(const T &t, const U &u) {
    { t <=> u } -> std::convertible_to<std::strong_ordering>;
}
inline std::strong_ordering
operator<=>(const Arc<T> &t,
            const Arc<U> &u) noexcept(noexcept(std::declval<const T &>() <=>
                                               std::declval<const U &>())) {
    if (!t) {
        if (!u)
            return std::strong_ordering::equal;
        return std::strong_ordering::less;
    }
    if (!u)
        return std::strong_ordering::greater;
    const T &t_v = *t;
    const U &u_v = *u;
    std::strong_ordering retval = t_v <=> u_v;
    return retval;
}

template <typename T, typename... Args>
requires std::constructible_from<T, Args...>
inline Arc<T> make_arc(Args &&...args) {
    return Arc<T>(std::make_shared<T>(std::forward<Args>(args)...));
}

template <typename T>
struct EnableArcFromThis : public std::enable_shared_from_this<T> {
protected:
    using std::enable_shared_from_this<T>::enable_shared_from_this;

public:
    Arc<T> arc_from_this() noexcept {
        return Arc<T>(this->shared_from_this());
    }
    Arc<const T> arc_from_this() const noexcept {
        return Arc<const T>(this->shared_from_this());
    }
};

template <typename T, typename U>
Arc<T> static_arc_cast(const Arc<U> &v) noexcept requires requires {
    static_cast<T *>((U *)nullptr);
}
{
    return Arc<T>(std::static_pointer_cast<T>(
        static_cast<const std::shared_ptr<U> &>(v)));
}

template <typename T, typename U>
Arc<T> static_arc_cast(Arc<U> &&v) noexcept requires requires {
    static_cast<T *>((U *)nullptr);
}
{
    return Arc<T>(std::static_pointer_cast<T>(
        static_cast<std::shared_ptr<U> &&>(std::move(v))));
}

template <typename T, typename U>
Arc<T> dynamic_arc_cast(const Arc<U> &v) noexcept requires requires {
    dynamic_cast<T *>((U *)nullptr);
}
{
    return Arc<T>(std::dynamic_pointer_cast<T>(
        static_cast<const std::shared_ptr<U> &>(v)));
}

template <typename T, typename U>
Arc<T> dynamic_arc_cast(Arc<U> &&v) noexcept requires requires {
    dynamic_cast<T *>((U *)nullptr);
}
{
    return Arc<T>(std::dynamic_pointer_cast<T>(
        static_cast<std::shared_ptr<U> &&>(std::move(v))));
}