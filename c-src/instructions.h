// SPDX-License-Identifier: LGPL-2.1-or-later
// See Notices.txt for copyright information
#pragma once

#include "arc.h"
#include "config_parser.h"
#include "instruction.h"
#include "register.h"
#include <algorithm>
#include <concepts>
#include <cstdint>
#include <ostream>
#include <string_view>

namespace instructions_helpers {
template <typename T>
concept InstructionLike = std::derived_from<T, Instruction> &&
    requires(const T &r) {
    { r.inputs() } -> std::same_as<std::span<const Register>>;
    { r.outputs() } -> std::same_as<std::span<const Register>>;
};

template <typename T>
concept InstructionLikeWithImmediates = InstructionLike<T> &&
    std::equality_comparable<typename T::ImmediateType> &&
    requires(const T &r) {
    {
        r.immediates()
        } -> std::same_as<std::span<const typename T::ImmediateType>>;
};

template <InstructionLike T>
constexpr bool is_equal_helper(const T &self, const Instruction &rhs) noexcept {
    auto rt = dynamic_cast<const T *>(&rhs);
    if (!rt)
        return false;
    auto compare_ranges = [](auto l, auto r) {
        return std::equal(l.begin(), l.end(), r.begin(), r.end());
    };
    if (!compare_ranges(self.outputs(), rt->outputs()))
        return false;
    if (!compare_ranges(self.inputs(), rt->inputs()))
        return false;
    if constexpr (InstructionLikeWithImmediates<T>) {
        if (!compare_ranges(self.immediates(), rt->immediates()))
            return false;
    }
    return true;
}
} // namespace instructions_helpers

#define DEFINE_INSTRUCTION_MEMBERS(class_name)                                 \
    Arc<Instruction> clone() const final {                                     \
        return make_arc<class_name>(*this);                                    \
    }                                                                          \
    constexpr Kind kind() const noexcept final {                               \
        return Kind::class_name;                                               \
    }                                                                          \
    static constexpr Kind KIND = Kind::class_name;                             \
    //

#define DEFINE_EQUAL(class_name, has_immediates)                               \
    constexpr bool is_equal(const Instruction &rhs) const final {              \
        using namespace instructions_helpers;                                  \
        return is_equal_helper<class_name>(*this, rhs);                        \
        static_assert(InstructionLikeWithImmediates<class_name> ==             \
                      has_immediates);                                         \
    }

#define DEFINE_ACCESSORS(name, array, index)                                   \
    constexpr const auto &name() const noexcept {                              \
        return array()[index];                                                 \
    }                                                                          \
    constexpr auto &name() noexcept {                                          \
        return array()[index];                                                 \
    }

#define DEFINE_IMMEDIATES(type, count)                                         \
public:                                                                        \
    typedef type ImmediateType;                                                \
                                                                               \
private:                                                                       \
    std::array<ImmediateType, count> m_immediates;                             \
                                                                               \
public:                                                                        \
    constexpr std::span<ImmediateType> immediates() noexcept {                 \
        return m_immediates;                                                   \
    }                                                                          \
    constexpr std::span<const ImmediateType> immediates() const noexcept {     \
        return m_immediates;                                                   \
    }

#define INSTRUCTION_KINDS()                                                    \
    INSTRUCTION_KIND(Mtctr, "mtctr")                                           \
    INSTRUCTION_KIND(Ldu, "ldu")                                               \
    INSTRUCTION_KIND(Std, "std")                                               \
    INSTRUCTION_KIND(Addi, "addi")                                             \
    INSTRUCTION_KIND(Cmpdi, "cmpdi")                                           \
    INSTRUCTION_KIND(Bdnz, "bdnz")                                             \
    INSTRUCTION_KIND(Bne, "bne")                                               \
    //

#define INSTRUCTION_KIND(kind, name) kind,
enum class Instruction::Kind : std::size_t { INSTRUCTION_KINDS() };
#undef INSTRUCTION_KIND

#define INSTRUCTION_KIND(kind, name) {Kind::kind, name},
constexpr std::initializer_list<std::pair<Instruction::Kind, std::string_view>>
    Instruction::kind_names = {INSTRUCTION_KINDS()};
#undef INSTRUCTION_KIND

constexpr std::string_view Instruction::name(Kind kind) noexcept {
#define INSTRUCTION_KIND(kind, name)                                           \
    case Kind::kind:                                                           \
        return name;
    switch (kind) { INSTRUCTION_KINDS() }
#undef INSTRUCTION_KIND
    assert(false);
}

namespace instructions {
class Mtctr final : public InstructionHelper<1, 1> {
public:
    constexpr Mtctr(Register ctr, Register rs) noexcept
        : InstructionHelper<1, 1>({ctr}, {rs}) {}
    constexpr explicit Mtctr(Register rs) noexcept : Mtctr(ISARegs::CTR, rs) {}
    DEFINE_INSTRUCTION_MEMBERS(Mtctr)
    DEFINE_EQUAL(Mtctr, false)
    DEFINE_ACCESSORS(ctr, outputs, 0)
    DEFINE_ACCESSORS(rs, inputs, 0)
    void write(std::ostream &os) const final {
        if (ctr() == ISARegs::CTR)
            write_impl(os, name(), {}, inputs());
        else
            write_impl(os, name(), outputs(), inputs());
    }
};

class Ldu final : public InstructionHelper<2, 1> {
public:
    constexpr Ldu(Register rt, std::int16_t offset, Register ra) noexcept
        : Ldu(rt, offset, ra, ra) {}
    constexpr Ldu(Register rt, std::int16_t offset, Register ra_input,
                  Register ra_output) noexcept
        : InstructionHelper<2, 1>({rt, ra_output}, {ra_input}), //
          m_immediates{offset} {}
    DEFINE_INSTRUCTION_MEMBERS(Ldu)
    DEFINE_EQUAL(Ldu, true)
    DEFINE_IMMEDIATES(std::int16_t, 1)
    DEFINE_ACCESSORS(rt, outputs, 0)
    DEFINE_ACCESSORS(ra_output, outputs, 1)
    DEFINE_ACCESSORS(ra_input, inputs, 0)
    DEFINE_ACCESSORS(offset, immediates, 0)
    void write(std::ostream &os) const final {
        os << name() << " " << rt() << ", " << offset() << "(" << ra_input();
        if (ra_input() != ra_output())
            os << " -> " << ra_output();
        os << ")";
    }
};

static_assert(instructions_helpers::InstructionLikeWithImmediates<Ldu>);

class Std final : public InstructionHelper<0, 2> {
public:
    constexpr Std(Register rs, std::int16_t offset, Register ra) noexcept
        : InstructionHelper({}, {rs, ra}), m_immediates{offset} {}
    DEFINE_INSTRUCTION_MEMBERS(Std)
    DEFINE_EQUAL(Std, true)
    DEFINE_IMMEDIATES(std::int16_t, 1)
    DEFINE_ACCESSORS(offset, immediates, 0)
    DEFINE_ACCESSORS(rs, inputs, 0)
    DEFINE_ACCESSORS(ra, inputs, 1)
    void write(std::ostream &os) const final {
        os << name() << " " << rs() << ", " << offset() << "(" << ra() << ")";
    }
};

class Addi final : public InstructionHelper<1, 1> {
public:
    constexpr Addi(Register rt, Register ra, std::int16_t immediate) noexcept
        : InstructionHelper({rt}, {ra}), m_immediates{immediate} {}
    DEFINE_INSTRUCTION_MEMBERS(Addi)
    DEFINE_EQUAL(Addi, true)
    DEFINE_IMMEDIATES(std::int16_t, 1)
    DEFINE_ACCESSORS(immediate, immediates, 0)
    DEFINE_ACCESSORS(rt, outputs, 0)
    DEFINE_ACCESSORS(ra, inputs, 0)
    void write(std::ostream &os) const final {
        write_impl(os, name(), outputs(), inputs(), immediates());
    }
};

class Cmpdi final : public InstructionHelper<1, 1> {
public:
    constexpr Cmpdi(Register cr, Register ra, std::int16_t immediate) noexcept
        : InstructionHelper({cr}, {ra}), m_immediates{immediate} {}
    DEFINE_INSTRUCTION_MEMBERS(Cmpdi)
    DEFINE_EQUAL(Cmpdi, true)
    DEFINE_IMMEDIATES(std::int16_t, 1)
    DEFINE_ACCESSORS(immediate, immediates, 0)
    DEFINE_ACCESSORS(cr, outputs, 0)
    DEFINE_ACCESSORS(ra, inputs, 0)
    void write(std::ostream &os) const final {
        if (cr() == ISARegs::CR0) {
            write_impl(os, name(), {}, inputs(), immediates());
        } else {
            write_impl(os, name(), outputs(), inputs(), immediates());
        }
    }
};

template <std::semiregular TA = std::int16_t>
requires requires(std::ostream &os, const TA &target_addr) {
    (void)(os << target_addr);
}
class Bdnz final : public InstructionHelper<1, 1> {
public:
    typedef TA TargetAddress;
    constexpr explicit Bdnz(TargetAddress target_addr) noexcept
        : Bdnz(ISARegs::CTR, ISARegs::CTR, std::move(target_addr)) {}
    constexpr Bdnz(Register ctr_output, Register ctr_input,
                   TargetAddress target_addr) noexcept
        : InstructionHelper<1, 1>({ctr_output}, {ctr_input}),
          m_immediates{std::move(target_addr)} {}
    DEFINE_INSTRUCTION_MEMBERS(Bdnz)
    DEFINE_EQUAL(Bdnz, true)
    DEFINE_IMMEDIATES(TargetAddress, 1)
    DEFINE_ACCESSORS(ctr_output, outputs, 0)
    DEFINE_ACCESSORS(ctr_input, inputs, 0)
    DEFINE_ACCESSORS(target_addr, immediates, 0)
    void write(std::ostream &os) const final {
        if (ctr_input() == ctr_output() && ctr_input() == ISARegs::CTR)
            write_impl(os, name(), {}, {},
                       std::span(std::array{target_addr()}));
        else
            write_impl(os, name(), outputs(), inputs(),
                       std::span(std::array{target_addr()}));
    }
};

template <std::semiregular TA = std::int16_t>
requires requires(std::ostream &os, const TA &target_addr) {
    (void)(os << target_addr);
}
class Bne final : public InstructionHelper<0, 1> {
public:
    typedef TA TargetAddress;
    constexpr explicit Bne(TargetAddress target_addr) noexcept
        : Bne(ISARegs::CR0, std::move(target_addr)) {}
    constexpr Bne(Register cr, TargetAddress target_addr) noexcept
        : InstructionHelper<0, 1>({}, {cr}), m_immediates{
                                                 std::move(target_addr)} {}
    DEFINE_INSTRUCTION_MEMBERS(Bne)
    DEFINE_EQUAL(Bne, true)
    DEFINE_IMMEDIATES(TargetAddress, 1)
    DEFINE_ACCESSORS(cr, inputs, 0)
    DEFINE_ACCESSORS(target_addr, immediates, 0)
    void write(std::ostream &os) const final {
        if (cr() == ISARegs::CR0)
            write_impl(os, name(), {}, {},
                       std::span(std::array{target_addr()}));
        else
            write_impl(os, name(), outputs(), inputs(),
                       std::span(std::array{target_addr()}));
    }
};
} // namespace instructions

#undef DEFINE_ACCESSORS
#undef DEFINE_IMMEDIATES
#undef DEFINE_INSTRUCTION_MEMBERS
#undef DEFINE_EQUAL
#undef INSTRUCTION_KINDS
