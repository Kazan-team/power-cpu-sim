#include <type_traits>

namespace {
template <typename T>
struct TypeTag {};

template <typename A, typename B>
constexpr bool require_matching_types() {
    TypeTag<A> v{};
    v = TypeTag<B>{};
    return true;
}

#define ASSERT_MATCHING_TYPES(x, ...)                                          \
    static_assert(require_matching_types<x, __VA_ARGS__>())

#ifndef __cpp_lib_concepts // custom type_traits header used
ASSERT_MATCHING_TYPES(typename std::__common_reference_detail::common_cv<
                          const unsigned &, const unsigned &>::type1,
                      const unsigned &);

ASSERT_MATCHING_TYPES(
    decltype((std::declval<typename std::__common_reference_detail::common_cv<
                  const unsigned &, const unsigned &>::type1>())),
    const unsigned &);

ASSERT_MATCHING_TYPES(decltype(false ? std::declval<const unsigned &>()
                                     : std::declval<const unsigned &>()),
                      const unsigned &);

ASSERT_MATCHING_TYPES(
    typename std::__common_reference_detail::simple_common_reference<
        const unsigned &, const unsigned &>::type,
    const unsigned &);

ASSERT_MATCHING_TYPES(
    typename std::__common_reference_detail::merge_ref<int, float>::type,
    float);

ASSERT_MATCHING_TYPES(
    typename std::__common_reference_detail::merge_ref<int &, float>::type,
    float &);

ASSERT_MATCHING_TYPES(
    typename std::__common_reference_detail::merge_ref<int &&, float>::type,
    float &&);

ASSERT_MATCHING_TYPES(
    typename std::__common_reference_detail::merge_const<int, float>::type,
    float);

ASSERT_MATCHING_TYPES(typename std::__common_reference_detail::merge_const<
                          const int, float>::type,
                      const float);

ASSERT_MATCHING_TYPES(
    typename std::__common_reference_detail::merge_volatile<int, float>::type,
    float);

ASSERT_MATCHING_TYPES(typename std::__common_reference_detail::merge_volatile<
                          volatile int, float>::type,
                      volatile float);

ASSERT_MATCHING_TYPES(typename std::__common_reference_detail::merge_volatile<
                          volatile int, float>::type,
                      volatile float);

ASSERT_MATCHING_TYPES(typename std::__common_reference_detail::merge_cvref_to<
                          volatile int>::get_merged<float>,
                      volatile float);

ASSERT_MATCHING_TYPES(typename std::__common_reference_detail::merge_cvref_to<
                          volatile int &&>::get_merged<float>,
                      volatile float &&);

#endif

ASSERT_MATCHING_TYPES(
    std::common_reference_t<const unsigned &, const unsigned &>,
    const unsigned &);

ASSERT_MATCHING_TYPES(
    std::common_reference_t<const unsigned &, volatile unsigned &>,
    const volatile unsigned &);

ASSERT_MATCHING_TYPES(
    std::common_reference_t<const unsigned &, volatile unsigned &&>, unsigned);

ASSERT_MATCHING_TYPES(
    std::common_reference_t<const unsigned &&, volatile unsigned &&>,
    const volatile unsigned &&);

ASSERT_MATCHING_TYPES(std::common_reference_t<unsigned &&, unsigned &&>,
                      unsigned &&);

ASSERT_MATCHING_TYPES(std::common_reference_t<unsigned &&, unsigned &>,
                      const unsigned &);

struct A {};
struct B {};
struct C {};

} // namespace

template <template <typename> typename TQual,
          template <typename> typename UQual>
struct ::std::basic_common_reference<A, B, TQual, UQual> {
    using type = TQual<UQual<C>>;
};

namespace {
ASSERT_MATCHING_TYPES(std::common_reference_t<A &&, const B &>, const C &);
}