// SPDX-License-Identifier: LGPL-2.1-or-later
// See Notices.txt for copyright information
#pragma once

#include "arc.h"
#include "c_api.h"
#include <cassert>
#include <memory>
#include <string>
#include <string_view>
#include <variant>

class TrackingTableWriter;

class TrackedTableColumn final {
    friend class TrackingTableWriter;

private:
    std::size_t m_index;
    std::string m_title;
    std::string m_value;

public:
    explicit TrackedTableColumn(std::size_t index, std::string title) noexcept
        : m_index(index), m_title(std::move(title)), m_value() {}

    void set_value(std::string v) noexcept {
        m_value = std::move(v);
    }
    template <typename T>
    void set_value(const T &v) noexcept {
        std::string_view sv = v;
        m_value = sv;
    }
    std::size_t index() const noexcept {
        return m_index;
    }
};

class TrackingTableWriter;

class TrackingTableWriterBuilder final {
    friend class TrackingTableWriter;

private:
    Output output;
    std::vector<Arc<TrackedTableColumn>> columns;

public:
    explicit TrackingTableWriterBuilder(Output output) noexcept
        : output(std::move(output)) {}
    Arc<TrackedTableColumn> add_column(std::string title) noexcept {
        auto retval = make_arc<TrackedTableColumn>(columns.size(), title);
        columns.push_back(retval);
        return retval;
    }
    TrackingTableWriter build() &&noexcept;
};

class TrackingTableWriter final {
private:
    TableWriter writer;
    std::vector<Arc<TrackedTableColumn>> columns;
    std::vector<StrFFI> columns_ffi;

    TrackingTableWriter(TableWriter writer,
                        std::vector<Arc<TrackedTableColumn>> columns,
                        std::vector<StrFFI> columns_ffi) noexcept
        : writer(std::move(writer)), columns(std::move(columns)),
          columns_ffi(std::move(columns_ffi)) {}

    static TrackingTableWriter
    start(TrackingTableWriterBuilder builder) noexcept {
        std::vector<StrFFI> columns_ffi;
        columns_ffi.reserve(builder.columns.size());
        for (const auto &column : builder.columns) {
            columns_ffi.push_back(StrFFI{
                .utf8_text = (const std::uint8_t *)column->m_title.data(),
                .len = column->m_title.size(),
            });
        }
        auto writer = TableWriter::start(std::move(builder.output),
                                         std::span<StrFFI>(columns_ffi));
        columns_ffi.clear();
        return TrackingTableWriter(std::move(writer),
                                   std::move(builder.columns),
                                   std::move(columns_ffi));
    }

public:
    explicit TrackingTableWriter(TrackingTableWriterBuilder builder) noexcept
        : TrackingTableWriter(start(std::move(builder))) {}

    void write_row() noexcept {
        for (const auto &column : columns) {
            columns_ffi.push_back(StrFFI{
                .utf8_text = (const std::uint8_t *)column->m_value.data(),
                .len = column->m_value.size(),
            });
        }
        writer.write_row(std::span<StrFFI>(columns_ffi));
        columns_ffi.clear();
    }
    Output finish() noexcept {
        return writer.finish();
    }
};

inline TrackingTableWriter TrackingTableWriterBuilder::build() &&noexcept {
    return TrackingTableWriter(std::move(*this));
}
