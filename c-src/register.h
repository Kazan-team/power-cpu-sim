// SPDX-License-Identifier: LGPL-2.1-or-later
// See Notices.txt for copyright information
#pragma once

#include "config_parser.h"
#include "util.h"
#include <cassert>
#include <concepts>
#include <span>
#include <string>
#include <string_view>
#include <type_traits>
#include <vector>

class RegisterClass;

class Register final {
private:
    std::size_t m_index;

    // points to staticly allocated RegisterClass
    const RegisterClass *m_register_class;

public:
    constexpr explicit Register(std::size_t index,
                                const RegisterClass *register_class) noexcept;
    constexpr bool operator==(const Register &) const noexcept = default;
    constexpr std::string_view name() const noexcept;
    constexpr std::size_t index() const noexcept {
        return m_index;
    }
    constexpr const RegisterClass *register_class() const noexcept {
        return m_register_class;
    }
    friend std::ostream &operator<<(std::ostream &os, const Register &r) {
        return os << r.name();
    }
};

template <>
struct std::hash<Register> {
    constexpr std::size_t operator()(const Register &r) const noexcept {
        return hash_helper(r.index(), r.register_class());
    }
};

class RegisterClass {
private:
    std::size_t m_register_count;

public:
    constexpr explicit RegisterClass(std::size_t register_count) noexcept
        : m_register_count(register_count){};
    constexpr virtual ~RegisterClass() noexcept = default;
    constexpr virtual std::string_view
    get_name(std::size_t index) const noexcept = 0;
    constexpr std::size_t register_count() const noexcept {
        return m_register_count;
    }
};

template <typename T>
concept RegisterClassC = std::derived_from<T, RegisterClass> &&
    requires(const Register &reg) {
    { T::get() } -> std::same_as<const T *>;
};

constexpr Register::Register(std::size_t index,
                             const RegisterClass *register_class) noexcept
    : m_index(index), m_register_class(register_class) {
    assert(index < register_class->register_count());
}

constexpr std::string_view Register::name() const noexcept {
    return m_register_class->get_name(m_index);
}

template <RegisterClassC T>
class RegisterWithClass;

namespace config_parser {
template <RegisterClassC T>
struct ParserFor<RegisterWithClass<T>>;
}

template <RegisterClassC T>
class RegisterWithClass final {
    friend struct config_parser::ParserFor<RegisterWithClass<T>>;

private:
    Register reg;

private:
    static std::vector<std::pair<RegisterWithClass, std::string_view>>
    make_enumerants() {
        const RegisterClass *register_class = T::get();
        std::vector<std::pair<RegisterWithClass, std::string_view>> retval;
        auto register_count = register_class->register_count();
        retval.reserve(register_count);
        for (std::size_t i = 0; i < register_count; i++) {
            retval.push_back({RegisterWithClass<T>(Register(i, register_class)),
                              register_class->get_name(i)});
        }
        return retval;
    }

public:
    constexpr RegisterWithClass() noexcept : reg(0, T::get()) {}
    constexpr explicit RegisterWithClass(std::size_t index) noexcept
        : reg(index, T::get()) {}
    constexpr explicit RegisterWithClass(Register reg) noexcept : reg(reg) {
        assert(T::is_instance(reg));
    }
    constexpr operator Register() const noexcept {
        return reg;
    }
    constexpr bool
    operator==(const RegisterWithClass<T> &) const noexcept = default;
    constexpr std::string_view name() const noexcept {
        return reg.name();
    }
    constexpr std::size_t index() const noexcept {
        return reg.index();
    }
    static constexpr const RegisterClass *register_class() noexcept {
        return T::get();
    }
    friend std::ostream &operator<<(std::ostream &os,
                                    const RegisterWithClass<T> &r) {
        return os << r.reg;
    }
};

template <RegisterClassC T>
struct std::hash<RegisterWithClass<T>> {
    constexpr std::size_t
    operator()(const RegisterWithClass<T> &r) const noexcept {
        return std::hash<Register>()(r);
    }
};

namespace config_parser {
template <RegisterClassC T>
struct ParserFor<RegisterWithClass<T>> final {
    static ParseResult<RegisterWithClass<T>>
    parse(std::optional<TomlValue> toml, std::string_view field_name) {
        static auto enumerants = RegisterWithClass<T>::make_enumerants();
        return parse_enum<RegisterWithClass<T>>(toml, field_name, enumerants);
    }
};
} // namespace config_parser

struct ISARegs final : RegisterClass {
    static const ISARegs value;
#define DEFINE_REGS(M)                                                         \
    M(r0, R0)                                                                  \
    M(r1, R1)                                                                  \
    M(r2, R2)                                                                  \
    M(r3, R3)                                                                  \
    M(r4, R4)                                                                  \
    M(r5, R5)                                                                  \
    M(r6, R6)                                                                  \
    M(r7, R7)                                                                  \
    M(r8, R8)                                                                  \
    M(r9, R9)                                                                  \
    M(r10, R10)                                                                \
    M(r11, R11)                                                                \
    M(r12, R12)                                                                \
    M(r13, R13)                                                                \
    M(r14, R14)                                                                \
    M(r15, R15)                                                                \
    M(r16, R16)                                                                \
    M(r17, R17)                                                                \
    M(r18, R18)                                                                \
    M(r19, R19)                                                                \
    M(r20, R20)                                                                \
    M(r21, R21)                                                                \
    M(r22, R22)                                                                \
    M(r23, R23)                                                                \
    M(r24, R24)                                                                \
    M(r25, R25)                                                                \
    M(r26, R26)                                                                \
    M(r27, R27)                                                                \
    M(r28, R28)                                                                \
    M(r29, R29)                                                                \
    M(r30, R30)                                                                \
    M(r31, R31)                                                                \
    M(lr, LR)                                                                  \
    M(ctr, CTR)                                                                \
    M(cr0, CR0)                                                                \
    M(cr1, CR1)                                                                \
    M(cr2, CR2)                                                                \
    M(cr3, CR3)                                                                \
    M(cr4, CR4)                                                                \
    M(cr5, CR5)                                                                \
    M(cr6, CR6)                                                                \
    M(cr7, CR7)

private:
    static constexpr inline std::string_view names[] = {
#define DEFINE_REGS_HELPER(lc, uc) #lc,
        DEFINE_REGS(DEFINE_REGS_HELPER)
#undef DEFINE_REGS_HELPER
    };
    enum class ValuesHelper : std::size_t {
#define DEFINE_REGS_HELPER(lc, uc) uc,
        DEFINE_REGS(DEFINE_REGS_HELPER)
#undef DEFINE_REGS_HELPER
    };
    constexpr ISARegs() noexcept
        : RegisterClass(std::span(ISARegs::names).size()) {}
    ISARegs(const ISARegs &) = delete;
    ISARegs &operator=(const ISARegs &) = delete;

public:
    constexpr virtual std::string_view
    get_name(std::size_t index) const noexcept {
        auto names = std::span(ISARegs::names);
        assert(index < names.size());
        return names[index];
    }
#define DEFINE_REGS_HELPER(lc, uc) static const Register uc;
    DEFINE_REGS(DEFINE_REGS_HELPER)
#undef DEFINE_REGS_HELPER
    static constexpr const ISARegs *get() noexcept;
    static constexpr bool is_instance(Register reg) noexcept {
        return reg.register_class() == get();
    }
};

constexpr inline ISARegs ISARegs::value{};

constexpr const ISARegs *ISARegs::get() noexcept {
    return &value;
}

#define DEFINE_REGS_HELPER(lc, uc)                                             \
    constexpr inline Register ISARegs::uc =                                    \
        Register(static_cast<std::size_t>(ISARegs::ValuesHelper::uc), &value);
DEFINE_REGS(DEFINE_REGS_HELPER)
#undef DEFINE_REGS_HELPER
#undef DEFINE_REGS

using ISAReg = RegisterWithClass<ISARegs>;

static_assert(config_parser::Parsable<ISAReg>);

class RenamedRegs final : public RegisterClass {
private:
    static std::vector<std::string> make_reg_names() noexcept {
        constexpr std::size_t count = 256;
        std::vector<std::string> retval;
        retval.reserve(count);
        for (std::size_t i = 0; i < count; i++) {
            auto value = "h" + std::to_string(i);
            value.shrink_to_fit();
            retval.push_back(std::move(value));
        }
        retval.shrink_to_fit();
        return retval;
    }
    static const std::vector<std::string> &get_reg_names() {
        static const std::vector<std::string> reg_names = make_reg_names();
        return reg_names;
    }
    using RegisterClass::RegisterClass;
    RenamedRegs(const RenamedRegs &) = delete;
    RenamedRegs &operator=(const RenamedRegs &) = delete;

public:
    static const RenamedRegs *get() noexcept {
        static const RenamedRegs value(get_reg_names().size());
        return &value;
    }
    static std::size_t register_count() noexcept {
        return get()->RegisterClass::register_count();
    }
    static Register reg(std::size_t index) noexcept {
        return Register(index, get());
    }
    virtual std::string_view get_name(std::size_t index) const noexcept {
        auto &reg_names = get_reg_names();
        assert(index < reg_names.size());
        return reg_names[index];
    }
    static bool is_instance(Register reg) noexcept {
        return reg.register_class() == get();
    }
};

using RenamedReg = RegisterWithClass<RenamedRegs>;

static_assert(config_parser::Parsable<RenamedReg>);
