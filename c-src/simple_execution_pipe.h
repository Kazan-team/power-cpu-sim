// SPDX-License-Identifier: LGPL-2.1-or-later
// See Notices.txt for copyright information
#pragma once

#include "arc.h"
#include "config.h"
#include "pipe.h"
#include "simulator.h"

class SimpleExecutionPipeFactory;

class SimpleExecutionPipe final : public Pipe {
private:
    std::vector<
        simulator::State<std::vector<std::shared_ptr<InstructionState>>>>
        m_instructions_pipe;

public:
    explicit SimpleExecutionPipe(Arc<Pipe> input_pipe,
                                 Arc<const PipeFactory> factory,
                                 simulator::Simulator &simulator,
                                 const config::SimpleExecutionPipe &cfg)
        : Pipe(std::move(input_pipe), std::move(factory), simulator),
          m_instructions_pipe() {
        m_instructions_pipe.reserve(cfg.delay);
        for (std::size_t i = 0; i < cfg.delay; i++) {
            m_instructions_pipe.push_back(
                simulator::State<
                    std::vector<std::shared_ptr<InstructionState>>>(simulator));
        }
    }
    static Arc<const SimpleExecutionPipeFactory>
    make_factory(config::SimpleExecutionPipe cfg);
    simulator::StateChanged
    compute_next_state(simulator::Simulator &) noexcept final {
        if (m_instructions_pipe.empty())
            return simulator::StateChanged::Unchanged;
        auto input_instructions = pipe_input_instructions();
        std::span<const std::shared_ptr<InstructionState>> last_instructions =
            input_instructions.instructions;
        auto changed = simulator::StateChanged::Unchanged;
        for (auto &state : m_instructions_pipe) {
            for (auto &instruction : last_instructions) {
                changed |= instruction->state.set_next("Executing");
            }
            changed |= state.set_next(std::vector(last_instructions.begin(),
                                                  last_instructions.end()));
            last_instructions = state.current();
        }
        return changed;
    }
    PipeInstructionsRef pipe_output_instructions() const noexcept final {
        if (m_instructions_pipe.empty())
            return pipe_input_instructions();
        return PipeInstructionsRef{.instructions =
                                       m_instructions_pipe.back().current(),
                                   .pipe_ref = arc_from_this()};
    }
};

class SimpleExecutionPipeFactory final
    : public PipeFactory,
      public EnableArcFromThis<SimpleExecutionPipeFactory> {
private:
    config::SimpleExecutionPipe m_cfg;

public:
    explicit SimpleExecutionPipeFactory(
        config::SimpleExecutionPipe cfg) noexcept
        : m_cfg(std::move(cfg)) {}
    constexpr bool is_instruction_handled(
        const Instruction &instruction) const noexcept final {
        if (m_cfg.filter_by)
            return m_cfg.filter_by->contains(instruction.kind());
        return true;
    }
    Arc<Pipe> create(Arc<Pipe> input_pipe,
                     simulator::Simulator &simulator) const final {
        Arc<const PipeFactory> factory = this->arc_from_this();
        auto pipe = make_arc<SimpleExecutionPipe>(
            std::move(input_pipe), std::move(factory), simulator, m_cfg);
        simulator.insert(pipe);
        return pipe;
    }
};

inline Arc<const SimpleExecutionPipeFactory>
SimpleExecutionPipe::make_factory(config::SimpleExecutionPipe cfg) {
    return make_arc<SimpleExecutionPipeFactory>(std::move(cfg));
}