// SPDX-License-Identifier: LGPL-2.1-or-later
// See Notices.txt for copyright information
#pragma once

#include "arc.h"
#include "pipe.h"
#include "register.h"
#include "rename_pipe.h"
#include "simulator.h"
#include <span>
#include <type_traits>
#include <vector>

class FinalPipe final : public Pipe {
private:
    simulator::State<std::vector<Register>> m_freed_registers;

public:
    explicit FinalPipe(Arc<Pipe> input_pipe, Arc<const PipeFactory> factory,
                       simulator::Simulator &simulator) noexcept
        : Pipe(std::move(input_pipe), std::move(factory), simulator),
          m_freed_registers(simulator) {}
    RenamePipe::FreedRegisters freed_registers() const noexcept {
        return {.registers = m_freed_registers.current(),
                .pipe_ref = arc_from_this()};
    }
    simulator::StateChanged
    compute_next_state(simulator::Simulator &) noexcept final {
        auto changed = simulator::StateChanged::Unchanged;
        std::vector<Register> freed_registers;
        auto input_instructions = pipe_input_instructions();
        for (auto &instruction : input_instructions.instructions) {
            changed |= instruction->state.set_next({});
            for (auto &reg : instruction->instruction.current()->inputs())
                freed_registers.push_back(reg);
            for (auto &reg : instruction->instruction.current()->outputs())
                freed_registers.push_back(reg);
        }
        changed |= m_freed_registers.set_next(std::move(freed_registers));
        return changed;
    }
    PipeInstructionsRef pipe_output_instructions() const override {
        return PipeInstructionsRef::empty();
    }
    static Arc<const PipeFactory> make_factory();
};

inline Arc<const PipeFactory> FinalPipe::make_factory() {
    return PipeFactory::make_factory_for<FinalPipe, Instruction>();
}
