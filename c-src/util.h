// SPDX-License-Identifier: LGPL-2.1-or-later
// See Notices.txt for copyright information
#pragma once

#include <concepts>
#include <cstddef>
#include <cstdint>
#include <utility>

namespace detail {
constexpr std::uint64_t mul_high(std::uint64_t a, std::uint64_t b) noexcept {
    typedef unsigned __int128 uint128_t;
    return static_cast<uint128_t>(a) * static_cast<uint128_t>(b) >> 64;
}
} // namespace detail

struct Hasher final {
    // algorithm derived from aHash fallback_hash
    std::uint64_t state = 0x243F6A8885A308D3ULL;
    constexpr Hasher() noexcept {}
    constexpr void operator()(std::uint64_t v) noexcept {
        state = detail::mul_high(state ^ v, 6364136223846793005ULL);
    }
    constexpr explicit operator std::uint64_t() const noexcept {
        return state + 0x13198A2E03707344ULL;
    }
};

template <typename T>
concept Hashable = requires(const T &v) {
    { std::hash<T>()(v) } -> std::same_as<std::size_t>;
};

template <Hashable... Args>
constexpr std::uint64_t hash_helper(const Args &...args) noexcept(
    (noexcept(std::hash<Args>()(args)) && ...)) {
    Hasher hasher;
    auto hash_helper = [&](std::size_t v) { hasher(v); };
    (hash_helper(std::hash<Args>()(args)), ...);
    return static_cast<std::uint64_t>(hasher);
}
