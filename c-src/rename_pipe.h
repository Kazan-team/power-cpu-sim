// SPDX-License-Identifier: LGPL-2.1-or-later
// See Notices.txt for copyright information
#pragma once

#include "arc.h"
#include "c_api.h"
#include "config.h"
#include "instruction.h"
#include "instruction_state.h"
#include "pipe.h"
#include "register.h"
#include "scheduler.h"
#include "simulator.h"
#include <deque>
#include <functional>
#include <memory>
#include <optional>
#include <span>
#include <sstream>
#include <unordered_map>
#include <vector>

class RenameState final {
private:
    std::unordered_map<ISAReg, RenamedReg> m_isa_to_hw_reg_map;
    // map from hardware registers to reference counts
    std::unordered_map<RenamedReg, std::size_t> m_hw_reg_reference_counts;
    std::deque<RenamedReg> m_unallocated_hw_regs;
    std::deque<std::shared_ptr<InstructionState>> m_queued_input_instructions;
    std::vector<std::shared_ptr<InstructionState>> m_output_instructions;

private:
    std::size_t &get_hw_reg_reference_count(const RenamedReg &hw_reg) noexcept {
        auto iter = m_hw_reg_reference_counts.find(hw_reg);
        assert(iter != m_hw_reg_reference_counts.end());
        return iter->second;
    }

public:
    RenameState() noexcept : RenameState(config::RenamePipe{}) {}
    explicit RenameState(const config::RenamePipe &cfg) noexcept
        : m_isa_to_hw_reg_map(std::move(cfg.initial_rename_map)),
          m_hw_reg_reference_counts(), m_unallocated_hw_regs(),
          m_queued_input_instructions(), m_output_instructions() {
        assert(cfg.hw_reg_count <= RenamedRegs::register_count());
        for (auto &[isa_reg, hw_reg] : m_isa_to_hw_reg_map) {
            assert(hw_reg.index() < cfg.hw_reg_count);
            auto &reference_count =
                m_hw_reg_reference_counts.emplace(hw_reg, 0).first->second;
            // have to increment separately to handle duplicate registers
            reference_count++;
        }
        for (std::size_t i = 0; i < cfg.hw_reg_count; i++) {
            auto reg = RenamedReg(i);
            bool inserted = m_hw_reg_reference_counts.emplace(reg, 0).second;
            if (inserted)
                m_unallocated_hw_regs.push_back(reg);
        }
    }
    bool operator==(const RenameState &) const noexcept = default;
    std::optional<RenamedReg>
    get_hw_reg_for_isa_reg(const ISAReg &isa_reg) const noexcept {
        auto iter = m_isa_to_hw_reg_map.find(isa_reg);
        if (iter == m_isa_to_hw_reg_map.end())
            return std::nullopt;
        return iter->second;
    }
    void set_hw_reg_for_isa_reg(const ISAReg &isa_reg,
                                const RenamedReg &hw_reg) noexcept {
        auto old_hw_reg = get_hw_reg_for_isa_reg(isa_reg);
        m_isa_to_hw_reg_map.insert_or_assign(isa_reg, hw_reg);
        if (old_hw_reg)
            dec_hw_reg_ref(*old_hw_reg);
    }
    void dec_hw_reg_ref(const RenamedReg &hw_reg) noexcept {
        auto &reference_count = m_hw_reg_reference_counts.at(std::move(hw_reg));
        assert(reference_count > 0);
        reference_count--;
        if (reference_count == 0) {
            m_unallocated_hw_regs.push_back(hw_reg);
        }
    }
    void inc_hw_reg_ref(const RenamedReg &hw_reg) noexcept {
        auto &reference_count = m_hw_reg_reference_counts.at(std::move(hw_reg));
        reference_count++;
    }
    std::size_t free_hw_reg_count() const noexcept {
        return m_unallocated_hw_regs.size();
    }
    std::optional<RenamedReg> allocate_hw_reg() noexcept {
        if (m_unallocated_hw_regs.empty()) {
            return std::nullopt;
        }
        auto retval = m_unallocated_hw_regs.front();
        m_unallocated_hw_regs.pop_front();
        inc_hw_reg_ref(retval);
        return retval;
    }
    std::deque<std::shared_ptr<InstructionState>> &
    queued_input_instructions() noexcept {
        return m_queued_input_instructions;
    }
    const std::deque<std::shared_ptr<InstructionState>> &
    queued_input_instructions() const noexcept {
        return m_queued_input_instructions;
    }
    std::vector<std::shared_ptr<InstructionState>> &
    output_instructions() noexcept {
        return m_output_instructions;
    }
    const std::vector<std::shared_ptr<InstructionState>> &
    output_instructions() const noexcept {
        return m_output_instructions;
    }
};

class RenamePipe final : public Pipe {
public:
    struct FreedRegisters final {
        /// the list of hw registers that should have
        /// `RenameState::dec_hw_reg_ref` called for them.
        std::span<const Register> registers;
        Arc<const void> pipe_ref;
    };
    using GetFreedRegisters = std::function<FreedRegisters()>;

private:
    simulator::State<RenameState> m_state;
    GetFreedRegisters m_get_freed_registers;

public:
    explicit RenamePipe(Arc<Pipe> input_pipe, Arc<const PipeFactory> factory,
                        simulator::Simulator &simulator,
                        GetFreedRegisters get_freed_registers,
                        const config::RenamePipe &cfg) noexcept
        : Pipe(std::move(input_pipe), std::move(factory), simulator),
          m_state(simulator, RenameState(cfg)),
          m_get_freed_registers(std::move(get_freed_registers)) {}
    static Arc<const PipeFactory>
    make_factory(GetFreedRegisters get_freed_registers,
                 const config::RenamePipe &cfg) {
        return PipeFactory::make_factory_for<RenamePipe, Instruction>(
            get_freed_registers, cfg);
    }
    void
    set_get_freed_registers(GetFreedRegisters get_freed_registers) noexcept {
        m_get_freed_registers = get_freed_registers;
    }
    PipeInstructionsRef pipe_output_instructions() const noexcept final {
        return {.instructions =
                    std::span(m_state.current().output_instructions()),
                .pipe_ref = arc_from_this()};
    }
    simulator::StateChanged
    compute_next_state(simulator::Simulator &) noexcept final {
        RenameState state = m_state.current();
        state.output_instructions().clear();
        auto input_instructions = pipe_input_instructions();
        for (auto instruction : input_instructions.instructions) {
            state.queued_input_instructions().push_back(instruction);
        }
        if (m_get_freed_registers) {
            auto freed_registers = m_get_freed_registers();
            for (auto &reg : freed_registers.registers) {
                state.dec_hw_reg_ref(RenamedReg(reg));
            }
        } else {
            console_log("WARNING: m_get_freed_registers not set");
        }
        auto changed = simulator::StateChanged::Unchanged;
        while (!state.queued_input_instructions().empty()) {
            auto instruction = state.queued_input_instructions().front();
            std::size_t needed_hw_reg_count =
                instruction->instruction.current()->outputs().size();
            for (auto &reg : instruction->instruction.current()->inputs()) {
                if (!state.get_hw_reg_for_isa_reg(ISAReg(reg)))
                    needed_hw_reg_count++;
            }
            if (needed_hw_reg_count > state.free_hw_reg_count()) {
                console_log("ran out of hw registers");
                break;
            }
            auto renamed_instruction =
                instruction->instruction.current()->clone();
            for (auto &reg : renamed_instruction->inputs()) {
                auto hw_reg = state.get_hw_reg_for_isa_reg(ISAReg(reg));
                if (!hw_reg) {
                    hw_reg = state.allocate_hw_reg();
                    assert(hw_reg); // already checked for enough regs above
                    state.set_hw_reg_for_isa_reg(ISAReg(reg), *hw_reg);
                }
                state.inc_hw_reg_ref(*hw_reg);
                reg = *hw_reg;
            }
            for (auto &reg : renamed_instruction->outputs()) {
                auto hw_reg = state.allocate_hw_reg();
                assert(hw_reg); // already checked for enough regs above
                state.inc_hw_reg_ref(*hw_reg);
                state.set_hw_reg_for_isa_reg(ISAReg(reg), *hw_reg);
                reg = *hw_reg;
            }
            changed |= instruction->instruction.set_next(renamed_instruction);
            std::ostringstream ss;
            ss << "Renamed: " << *renamed_instruction;
            changed |= instruction->state.set_next(std::move(ss).str());
            state.output_instructions().push_back(instruction);
            state.queued_input_instructions().pop_front();
        }
        for (auto &instruction : state.queued_input_instructions()) {
            changed |=
                instruction->state.set_next("Wait: not enough free regs");
        }
        changed |= m_state.set_next(std::move(state));
        return changed;
    }
};