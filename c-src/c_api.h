// SPDX-License-Identifier: LGPL-2.1-or-later
// See Notices.txt for copyright information
#pragma once

#include "c_api_generated.h"
#include <cassert>
#include <compare>
#include <concepts>
#include <cstddef>
#include <cstdint>
#include <iterator>
#include <optional>
#include <span>
#include <string_view>
#include <type_traits>
#include <utility>
#include <vector>

inline void console_log(const char *utf8_text) noexcept {
    console_log_cstr(utf8_text);
}

inline void console_log(std::string_view text) noexcept {
    console_log((const std::uint8_t *)text.data(), text.size());
}

class TableWriter;

class Output final {
    friend class TableWriter;

private:
    OutputFFI *value;
    const TableWriterVTable *vtable;

public:
    Output(OutputFFI *value, const TableWriterVTable *vtable) noexcept
        : value(value), vtable(vtable) {}
    Output(const Output &) = delete;
    Output &operator=(const Output &) = delete;
    Output(Output &&other) noexcept : value(other.value), vtable(other.vtable) {
        other.value = nullptr;
    }
    Output &operator=(Output &&other) noexcept {
        if (value)
            vtable->output_drop(value);
        value = other.value;
        vtable = other.vtable;
        other.value = nullptr;
        return *this;
    }
    ~Output() noexcept {
        if (value)
            vtable->output_drop(value);
    }
    void write_plain_text(std::string_view utf8_text) noexcept {
        if (value)
            vtable->write_plain_text(value,
                                     (const std::uint8_t *)utf8_text.data(),
                                     utf8_text.size());
    }
};

class TableWriter final {
    friend class Output;

private:
    TableWriterFFI *value;
    const TableWriterVTable *vtable;

    TableWriter(TableWriterFFI *value, const TableWriterVTable *vtable) noexcept
        : value(value), vtable(vtable) {}

public:
    TableWriter(const TableWriter &) = delete;
    TableWriter &operator=(const TableWriter &) = delete;
    TableWriter(TableWriter &&other) noexcept
        : value(other.value), vtable(other.vtable) {
        other.value = nullptr;
    }
    TableWriter &operator=(TableWriter &&other) noexcept {
        if (value)
            vtable->table_writer_drop(value);
        value = other.value;
        vtable = other.vtable;
        other.value = nullptr;
        return *this;
    }
    ~TableWriter() noexcept {
        if (value)
            vtable->table_writer_drop(value);
    }
    static TableWriter start(Output output,
                             std::span<StrFFI> header_columns) noexcept {
        if (output.value) {
            auto output_value = output.value;
            output.value = nullptr; // prevent calling destructor since start
                                    // takes ownership of output
            auto table_writer = output.vtable->start(
                output_value, header_columns.data(), header_columns.size());
            return TableWriter(table_writer, output.vtable);
        } else {
            return TableWriter(nullptr, output.vtable);
        }
    }
    template <typename T>
    static TableWriter start(Output output,
                             std::span<T> header_columns) noexcept {
        std::vector<StrFFI> header_columns_ffi;
        header_columns_ffi.reserve(header_columns.size());
        for (std::string_view header_column : header_columns) {
            header_columns_ffi.push_back(StrFFI{
                .utf8_text = (const std::uint8_t *)header_column.data(),
                .len = header_column.size(),
            });
        }
        return start(std::move(output), header_columns_ffi);
    }
    void write_row(std::span<StrFFI> columns) noexcept {
        if (value) {
            vtable->write_row(value, columns.data(), columns.size());
        }
    }
    template <typename T>
    void write_row(std::span<T> columns) noexcept {
        std::vector<StrFFI> columns_ffi;
        columns_ffi.reserve(columns.size());
        for (const std::string_view &column : columns) {
            columns_ffi.push_back(StrFFI{
                .utf8_text = (const std::uint8_t *)column.data(),
                .len = column.size(),
            });
        }
        write_row(columns_ffi);
    }
    /// moves from `*this`
    Output finish() noexcept {
        if (value) {
            auto table_writer = value;
            value = nullptr; // prevent calling destructor since finish takes
                             // ownership of table_writer
            auto output = vtable->finish(table_writer);
            return Output(output, vtable);
        } else {
            return Output(nullptr, vtable);
        }
    }
};

class TomlArray;
class TomlTable;

class TomlValue final {
    friend class TomlArray;
    friend class TomlTable;

public:
    using Kind = TomlValueFFI::Tag;

private:
    const TomlValueFFI *m_value;
    std::string_view m_string_value;
    static inline constexpr TomlValueFFI m_default_value = {
        .tag = Kind::Integer,
        .integer = {._0 = 0},
    };
    static inline constexpr TomlValueFFI m_string_toml_value = {
        .tag = Kind::String,
        .string = {._0 = {.utf8_text = nullptr, .len = 0}},
    };

public:
    constexpr TomlValue() noexcept
        : m_value(&m_default_value), m_string_value() {}
    constexpr explicit TomlValue(const TomlValueFFI *value) noexcept
        : m_value(value), m_string_value() {
        assert(value);
        if (is_string())
            m_string_value =
                std::string_view((const char *)m_value->string._0.utf8_text,
                                 m_value->string._0.len);
    }
    constexpr explicit TomlValue(std::string_view value) noexcept
        : m_value(&m_string_toml_value), m_string_value(value) {}
    constexpr Kind kind() const noexcept {
        return m_value->tag;
    }
    constexpr bool is_integer() const noexcept {
        return kind() == Kind::Integer;
    }
    constexpr bool is_string() const noexcept {
        return kind() == Kind::String;
    }
    constexpr bool is_float() const noexcept {
        return kind() == Kind::Float;
    }
    constexpr bool is_boolean() const noexcept {
        return kind() == Kind::Boolean;
    }
    constexpr bool is_datetime() const noexcept {
        return kind() == Kind::Datetime;
    }
    constexpr bool is_array() const noexcept {
        return kind() == Kind::Array;
    }
    constexpr bool is_table() const noexcept {
        return kind() == Kind::Table;
    }
    constexpr std::int64_t get_integer() const noexcept {
        assert(is_integer());
        return m_value->integer._0;
    }
    constexpr std::string_view get_string() const noexcept {
        assert(is_string());
        return m_string_value;
    }
    constexpr double get_float() const noexcept {
        assert(is_float());
        return m_value->float_._0;
    }
    constexpr bool get_boolean() const noexcept {
        assert(is_boolean());
        return m_value->boolean._0;
    }
    constexpr std::string_view get_datetime() const noexcept {
        assert(is_datetime());
        return std::string_view((const char *)m_value->datetime._0.utf8_text,
                                m_value->datetime._0.len);
    }
    constexpr TomlArray get_array() const noexcept;
    constexpr TomlTable get_table() const noexcept;
    constexpr std::optional<std::int64_t> integer_opt() const noexcept {
        if (is_integer())
            return get_integer();
        return std::nullopt;
    }
    constexpr std::optional<std::string_view> string_opt() const noexcept {
        if (is_string())
            return get_string();
        return std::nullopt;
    }
    constexpr std::optional<double> float_opt() const noexcept {
        if (is_float())
            return get_float();
        return std::nullopt;
    }
    constexpr std::optional<bool> boolean_opt() const noexcept {
        if (is_boolean())
            return get_boolean();
        return std::nullopt;
    }
    constexpr std::optional<std::string_view> datetime_opt() const noexcept {
        if (is_datetime())
            return get_datetime();
        return std::nullopt;
    }
    constexpr std::optional<TomlArray> array_opt() const noexcept;
    constexpr std::optional<TomlTable> table_opt() const noexcept;
};

namespace toml_detail {
template <typename T>
concept IteratorBehavior = std::semiregular<typename T::DataType> &&
    std::semiregular<std::remove_const_t<typename T::ValueType>> &&
    requires(typename T::DataType &d, const typename T::DataType &cd,
             std::remove_const_t<typename T::ValueType> &v) {
    typename T::DataType;
    typename T::ValueType;
    typename T::AccessKey;
    { T::length(cd) }
    noexcept->std::same_as<std::size_t>;
    { v = T::get(cd, (std::size_t)0) }
    noexcept;
};

template <IteratorBehavior B>
struct ReverseBehavior final {
    using DataType = typename B::DataType;
    using ValueType = typename B::ValueType;
    using AccessKey = typename B::AccessKey;
    static constexpr std::size_t length(const DataType &data) noexcept {
        return B::length(data);
    }
    static constexpr decltype(auto) get(const DataType &data,
                                        std::size_t index) noexcept {
        std::size_t length = B::length(data);
        assert(index < length);
        return B::get(data, length - index - (std::size_t)1);
    }
};

template <IteratorBehavior B>
class Iterator {
public:
    using difference_type = std::ptrdiff_t;
    using value_type = typename B::ValueType;
    using pointer = typename B::ValueType *;
    using reference = typename B::ValueType;
    // can't be random access due to multipass guarantee on forward
    // iterators
    using iterator_category = std::input_iterator_tag;

private:
    typename B::DataType m_data;
    std::size_t m_index;
    mutable std::remove_const_t<typename B::ValueType> m_value;

public:
    constexpr explicit Iterator(typename B::DataType data, std::size_t index,
                                typename B::AccessKey) noexcept
        : m_data(std::move(data)), m_index(index) {}
    constexpr typename B::DataType &data(typename B::AccessKey) noexcept {
        return m_data;
    }
    constexpr std::size_t &index(typename B::AccessKey) noexcept {
        return m_index;
    }
    constexpr std::size_t index(typename B::AccessKey) const noexcept {
        return m_index;
    }
    constexpr Iterator() noexcept : m_data(), m_index(0), m_value() {}
    constexpr Iterator &operator++() noexcept {
        return operator+=(1);
    }
    constexpr Iterator operator++(int) noexcept {
        auto retval = *this;
        operator++();
        return retval;
    }
    constexpr Iterator &operator--() noexcept {
        return operator-=(1);
    }
    constexpr Iterator operator--(int) noexcept {
        auto retval = *this;
        operator--();
        return retval;
    }
    constexpr typename B::ValueType operator*() const noexcept {
        return *operator->();
    }
    constexpr typename B::ValueType *operator->() const noexcept {
        assert(m_index < B::length(m_data));
        m_value = B::get(m_data, m_index);
        return &m_value;
    }
    constexpr Iterator &operator+=(std::ptrdiff_t offset) noexcept {
        m_index += offset;
        assert(m_index <= B::length(m_data));
        return *this;
    }
    constexpr Iterator &operator-=(std::ptrdiff_t offset) noexcept {
        return operator+=(-offset);
    }
    constexpr typename B::ValueType
    operator[](std::ptrdiff_t offset) const noexcept {
        return *(*this + offset);
    }
    friend constexpr std::strong_ordering
    operator<=>(const Iterator &lhs, const Iterator &rhs) noexcept {
        return lhs.m_index <=> rhs.m_index;
    }
    friend constexpr bool operator==(const Iterator &lhs,
                                     const Iterator &rhs) noexcept {
        return lhs.m_index == rhs.m_index;
    }
    friend constexpr Iterator operator+(std::ptrdiff_t offset,
                                        Iterator iter) noexcept {
        iter += offset;
        return iter;
    }
    friend constexpr Iterator operator+(Iterator iter,
                                        std::ptrdiff_t offset) noexcept {
        iter += offset;
        return iter;
    }
    friend constexpr Iterator operator-(Iterator iter,
                                        std::ptrdiff_t offset) noexcept {
        iter -= offset;
        return iter;
    }
    friend constexpr std::ptrdiff_t operator-(Iterator lhs,
                                              Iterator rhs) noexcept {
        // subtract as size_t to avoid signed overflow UB
        return static_cast<std::ptrdiff_t>(lhs.m_index - rhs.m_index);
    }
};
} // namespace toml_detail

class TomlArray final {
    friend class TomlTable;

private:
    ConstSliceFFI<TomlValueFFI> m_value;

public:
    constexpr TomlArray() noexcept : m_value{.data = nullptr, .len = 0} {}
    constexpr explicit TomlArray(ConstSliceFFI<TomlValueFFI> value) noexcept
        : TomlArray() {
        if (value.len != 0) {
            assert(value.data);
            m_value = value;
        }
    }

private:
    class MyAccessKey final {
    private:
        friend class TomlArray;
        constexpr MyAccessKey() noexcept = default;
    };
    struct MyIteratorBehavior final {
        using AccessKey = MyAccessKey;
        using DataType = ConstSliceFFI<TomlValueFFI>;
        using ValueType = const TomlValue;
        static constexpr std::size_t
        length(ConstSliceFFI<TomlValueFFI> data) noexcept {
            return data.len;
        }
        static constexpr TomlValue get(ConstSliceFFI<TomlValueFFI> data,
                                       std::size_t index) noexcept {
            assert(index < data.len);
            return TomlValue(data.data + index);
        }
    };

public:
    using const_iterator = toml_detail::Iterator<MyIteratorBehavior>;
    using iterator = const_iterator;
    constexpr const_iterator cbegin() const noexcept {
        return const_iterator(m_value, 0, {});
    }
    constexpr const_iterator cend() const noexcept {
        return const_iterator(m_value, m_value.len, {});
    }
    constexpr const_iterator begin() const noexcept {
        return cbegin();
    }
    constexpr iterator begin() noexcept {
        return cbegin();
    }
    constexpr const_iterator end() const noexcept {
        return cend();
    }
    constexpr iterator end() noexcept {
        return cend();
    }
    using const_reverse_iterator =
        toml_detail::Iterator<toml_detail::ReverseBehavior<MyIteratorBehavior>>;
    using reverse_iterator = const_reverse_iterator;
    constexpr const_reverse_iterator crbegin() const noexcept {
        return const_reverse_iterator(m_value, 0, {});
    }
    constexpr const_reverse_iterator crend() const noexcept {
        return const_reverse_iterator(m_value, m_value.len, {});
    }
    constexpr const_reverse_iterator rbegin() const noexcept {
        return crbegin();
    }
    constexpr reverse_iterator rbegin() noexcept {
        return crbegin();
    }
    constexpr const_reverse_iterator rend() const noexcept {
        return crend();
    }
    constexpr reverse_iterator rend() noexcept {
        return crend();
    }
    constexpr TomlValue operator[](std::size_t index) const noexcept {
        return begin()[index];
    }
    constexpr std::size_t size() const noexcept {
        return m_value.len;
    }
    constexpr bool empty() const noexcept {
        return size() == 0;
    }
    constexpr TomlValue front() const noexcept {
        return *begin();
    }
    constexpr TomlValue back() const noexcept {
        return *rbegin();
    }
};

constexpr TomlArray TomlValue::get_array() const noexcept {
    assert(is_array());
    return TomlArray(m_value->array._0);
}

constexpr std::optional<TomlArray> TomlValue::array_opt() const noexcept {
    if (is_array())
        return get_array();
    return std::nullopt;
}

class TomlTableKeys final {
    friend class TomlTable;

private:
    ConstSliceFFI<StrFFI> m_value;

public:
    constexpr TomlTableKeys() noexcept : m_value{.data = nullptr, .len = 0} {}
    constexpr explicit TomlTableKeys(ConstSliceFFI<StrFFI> value) noexcept
        : TomlTableKeys() {
        if (value.len != 0) {
            assert(value.data);
            m_value = value;
        }
    }

private:
    class MyAccessKey final {
    private:
        friend class TomlTableKeys;
        constexpr MyAccessKey() noexcept = default;
    };
    struct MyIteratorBehavior final {
        using AccessKey = MyAccessKey;
        using DataType = ConstSliceFFI<StrFFI>;
        using ValueType = const std::string_view;
        static constexpr std::size_t
        length(ConstSliceFFI<StrFFI> data) noexcept {
            return data.len;
        }
        static constexpr std::string_view get(ConstSliceFFI<StrFFI> data,
                                              std::size_t index) noexcept {
            assert(index < data.len);
            return std::string_view((const char *)data.data[index].utf8_text,
                                    data.data[index].len);
        }
    };

public:
    using const_iterator = toml_detail::Iterator<MyIteratorBehavior>;
    using iterator = const_iterator;
    constexpr const_iterator cbegin() const noexcept {
        return const_iterator(m_value, 0, {});
    }
    constexpr const_iterator cend() const noexcept {
        return const_iterator(m_value, m_value.len, {});
    }
    constexpr const_iterator begin() const noexcept {
        return cbegin();
    }
    constexpr iterator begin() noexcept {
        return cbegin();
    }
    constexpr const_iterator end() const noexcept {
        return cend();
    }
    constexpr iterator end() noexcept {
        return cend();
    }
    using const_reverse_iterator =
        toml_detail::Iterator<toml_detail::ReverseBehavior<MyIteratorBehavior>>;
    using reverse_iterator = const_reverse_iterator;
    constexpr const_reverse_iterator crbegin() const noexcept {
        return const_reverse_iterator(m_value, 0, {});
    }
    constexpr const_reverse_iterator crend() const noexcept {
        return const_reverse_iterator(m_value, m_value.len, {});
    }
    constexpr const_reverse_iterator rbegin() const noexcept {
        return crbegin();
    }
    constexpr reverse_iterator rbegin() noexcept {
        return crbegin();
    }
    constexpr const_reverse_iterator rend() const noexcept {
        return crend();
    }
    constexpr reverse_iterator rend() noexcept {
        return crend();
    }
    constexpr std::string_view operator[](std::size_t index) const noexcept {
        return begin()[index];
    }
    constexpr std::size_t size() const noexcept {
        return m_value.len;
    }
    constexpr bool empty() const noexcept {
        return size() == 0;
    }
    constexpr std::string_view front() const noexcept {
        return *begin();
    }
    constexpr std::string_view back() const noexcept {
        return *rbegin();
    }
};

class TomlTable final {
private:
    TomlTableKeys m_keys;
    TomlArray m_values;

public:
    constexpr TomlTable() noexcept : m_keys(), m_values() {}
    constexpr explicit TomlTable(ConstSliceFFI<StrFFI> keys,
                                 ConstSliceFFI<TomlValueFFI> values) noexcept
        : m_keys(keys), m_values(values) {
        assert(keys.len == values.len);
    }

private:
    class MyAccessKey final {
    private:
        friend class TomlTable;
        constexpr MyAccessKey() noexcept = default;
    };
    struct MyIteratorBehavior final {
        using AccessKey = MyAccessKey;
        using DataType = std::pair<TomlTableKeys, TomlArray>;
        using ValueType = const std::pair<std::string_view, TomlValue>;
        static constexpr std::size_t
        length(std::pair<TomlTableKeys, TomlArray> data) noexcept {
            return data.first.size();
        }
        static constexpr std::pair<std::string_view, TomlValue>
        get(std::pair<TomlTableKeys, TomlArray> data,
            std::size_t index) noexcept {
            return {data.first[index], data.second[index]};
        }
    };

public:
    using const_iterator = toml_detail::Iterator<MyIteratorBehavior>;
    using iterator = const_iterator;
    constexpr const_iterator cbegin() const noexcept {
        return const_iterator({m_keys, m_values}, 0, {});
    }
    constexpr const_iterator cend() const noexcept {
        return const_iterator({m_keys, m_values}, size(), {});
    }
    constexpr const_iterator begin() const noexcept {
        return cbegin();
    }
    constexpr iterator begin() noexcept {
        return cbegin();
    }
    constexpr const_iterator end() const noexcept {
        return cend();
    }
    constexpr iterator end() noexcept {
        return cend();
    }
    using const_reverse_iterator =
        toml_detail::Iterator<toml_detail::ReverseBehavior<MyIteratorBehavior>>;
    using reverse_iterator = const_reverse_iterator;
    constexpr const_reverse_iterator crbegin() const noexcept {
        return const_reverse_iterator({m_keys, m_values}, 0, {});
    }
    constexpr const_reverse_iterator crend() const noexcept {
        return const_reverse_iterator({m_keys, m_values}, size(), {});
    }
    constexpr const_reverse_iterator rbegin() const noexcept {
        return crbegin();
    }
    constexpr reverse_iterator rbegin() noexcept {
        return crbegin();
    }
    constexpr const_reverse_iterator rend() const noexcept {
        return crend();
    }
    constexpr reverse_iterator rend() noexcept {
        return crend();
    }
    constexpr TomlTableKeys keys() const noexcept {
        return m_keys;
    }
    constexpr TomlArray values() const noexcept {
        return m_values;
    }
    constexpr iterator find(std::string_view key) const noexcept {
        auto retval = begin();
        // linear search is ok since config tables typically should have only a
        // few keys
        for (; retval != end(); ++retval) {
            if (retval->first == key)
                break;
        }
        return retval;
    }
    constexpr std::optional<TomlValue>
    get_opt(std::string_view key) const noexcept {
        auto iter = find(key);
        if (iter == end())
            return std::nullopt;
        return iter->second;
    }
    constexpr std::size_t size() const noexcept {
        return m_values.size();
    }
    constexpr bool empty() const noexcept {
        return size() == 0;
    }
};

constexpr TomlTable TomlValue::get_table() const noexcept {
    assert(is_table());
    return TomlTable(m_value->table.keys, m_value->table.values);
}

constexpr std::optional<TomlTable> TomlValue::table_opt() const noexcept {
    if (is_table())
        return get_table();
    return std::nullopt;
}
