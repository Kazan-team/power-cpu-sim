// SPDX-License-Identifier: LGPL-2.1-or-later
// See Notices.txt for copyright information
#pragma once

#include "arc.h"
#include "instruction.h"
#include "register.h"
#include "simulator.h"
#include "tracking_table_writer.h"
#include <array>
#include <cstdint>
#include <functional>
#include <memory>
#include <span>
#include <sstream>
#include <string>
#include <string_view>
#include <vector>

typedef std::uint64_t InstructionAddress;

struct InstructionState final {
    InstructionAddress address; // multiple issued instructions can have the
                                // same address, e.g. in a loop. They should not
                                // share InstructionState structs.
    simulator::State<Arc<Instruction>> instruction;
    simulator::State<std::string> state;
    Arc<TrackedTableColumn> table_column;

    // should be compared by pointer equality, not contents
    bool operator==(const InstructionState &) const noexcept = delete;
};
