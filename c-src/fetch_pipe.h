// SPDX-License-Identifier: LGPL-2.1-or-later
// See Notices.txt for copyright information
#pragma once

#include "arc.h"
#include "c_api.h"
#include "config.h"
#include "instruction.h"
#include "instruction_state.h"
#include "instruction_trace.h"
#include "pipe.h"
#include "simulator.h"
#include "tracking_table_writer.h"
#include <cassert>
#include <concepts>
#include <cstdint>
#include <initializer_list>
#include <memory>
#include <optional>
#include <ostream>
#include <span>
#include <sstream>
#include <string>
#include <vector>

struct FetchPipeState final {
    std::size_t instruction_trace_index = 0;
    std::vector<std::shared_ptr<InstructionState>> fetched_instructions{};
    bool operator==(const FetchPipeState &) const noexcept = default;
};

class FetchPipe final : public Pipe {
private:
    std::uint32_t m_max_fetch_width;
    std::uint32_t m_max_branches_per_cycle;
    Arc<const InstructionTrace> m_instruction_trace;
    simulator::State<FetchPipeState> m_state;

public:
    explicit FetchPipe(Arc<Pipe> input_pipe, Arc<const PipeFactory> factory,
                       simulator::Simulator &simulator,
                       const config::FetchPipe &cfg,
                       Arc<const InstructionTrace> instruction_trace)
        : Pipe(std::move(input_pipe), std::move(factory), simulator),
          m_max_fetch_width(cfg.max_fetch_width),
          m_max_branches_per_cycle(cfg.max_branches_per_cycle),
          m_instruction_trace(std::move(instruction_trace)),
          m_state(simulator,
                  {.instruction_trace_index = 0, .fetched_instructions = {}}) {}
    static Arc<const PipeFactory>
    make_factory(const config::FetchPipe &cfg,
                 Arc<const InstructionTrace> instruction_trace);
    std::size_t max_branches_per_cycle() const noexcept {
        return m_max_branches_per_cycle;
    }
    std::size_t max_fetch_width() const noexcept {
        return m_max_fetch_width;
    }
    const FetchPipeState &current_state() const noexcept {
        return m_state.current();
    }
    PipeInstructionsRef pipe_output_instructions() const noexcept final {
        return {.instructions = std::span(current_state().fetched_instructions),
                .pipe_ref = arc_from_this()};
    }
    simulator::StateChanged
    compute_next_state(simulator::Simulator &) noexcept final {
        auto &current = m_state.current();
        std::span<const std::shared_ptr<InstructionState>> instructions =
            m_instruction_trace->instructions();
        auto instruction_trace_index = current.instruction_trace_index;
        std::vector<std::shared_ptr<InstructionState>> fetched_instructions;
        auto changed = simulator::StateChanged::Unchanged;
        auto do_fetch = [&](std::shared_ptr<InstructionState> instruction) {
            changed |= instruction->state.set_next("Fetch");
            fetched_instructions.push_back(instruction);
            instruction_trace_index++;
        };
        if (instruction_trace_index < instructions.size())
            do_fetch(instructions[instruction_trace_index]);
        std::size_t branch_count = 0;
        while (instruction_trace_index < instructions.size()) {
            auto &instruction = instructions[instruction_trace_index];
            if (fetched_instructions.size() >= max_fetch_width()) {
                console_log("max_fetch_width reached");
                break;
            }
            auto &last_instruction = fetched_instructions.back();
            bool branches =
                last_instruction->instruction.current()->size_in_bytes() +
                    last_instruction->address !=
                instruction->address;
            if (branches) {
                console_log("branches " +
                            std::to_string(last_instruction->address) + " -> " +
                            std::to_string(instruction->address));
                if (++branch_count >= max_branches_per_cycle()) {
                    console_log("max_branches_per_cycle reached");
                    break;
                }
            }
            do_fetch(instruction);
        }
        changed |= m_state.set_next(
            {.instruction_trace_index = instruction_trace_index,
             .fetched_instructions = std::move(fetched_instructions)});
        return changed;
    }
};

inline Arc<const PipeFactory>
FetchPipe::make_factory(const config::FetchPipe &cfg,
                        Arc<const InstructionTrace> instruction_trace) {
    return PipeFactory::make_factory_for<FetchPipe, Instruction>(
        cfg, std::move(instruction_trace));
}
