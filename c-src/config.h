// SPDX-License-Identifier: LGPL-2.1-or-later
// See Notices.txt for copyright information
#pragma once

#include "arc.h"
#include "c_api.h"
#include "config_parser.h"
#include "instruction.h"
#include "register.h"
#include <cstdint>
#include <memory>
#include <string_view>
#include <tuple>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <variant>

namespace config {
using namespace config_parser;

class Pipe {
public:
    constexpr Pipe() noexcept = default;
    constexpr Pipe(const Pipe &) noexcept = default;
    constexpr Pipe(Pipe &&) noexcept = default;
    constexpr Pipe &operator=(const Pipe &) noexcept = default;
    constexpr Pipe &operator=(Pipe &&) noexcept = default;
    constexpr virtual ~Pipe() noexcept = default;
    template <typename Fn>
    constexpr void visit(Fn &&fn);
};
} // namespace config

template <>
struct config_parser::ParserFor<Arc<config::Pipe>> final {
    static ParseResult<Arc<config::Pipe>> parse(std::optional<TomlValue> toml,
                                                std::string_view field_name);
};

namespace config {
static_assert(Parsable<Arc<Pipe>>);

struct FetchPipe final : public Pipe {
    NonZero<std::uint32_t> max_fetch_width;
    NonZero<std::uint32_t> max_branches_per_cycle;

    static constexpr std::tuple FIELDS = {
        Field{&FetchPipe::max_fetch_width, "max_fetch_width"},
        Field{&FetchPipe::max_branches_per_cycle, "max_branches_per_cycle"},
    };
    static constexpr std::string_view NAME = "FetchPipe";
};

static_assert(Parsable<FetchPipe>);

struct RenamePipe final : public Pipe {
    NonZero<std::size_t> hw_reg_count = NonZero(RenamedRegs::register_count());
    std::unordered_map<ISAReg, RenamedReg> initial_rename_map;

    static constexpr std::tuple FIELDS = {
        Field{&RenamePipe::hw_reg_count, "hw_reg_count"},
        Field{&RenamePipe::initial_rename_map, "initial_rename_map"},
    };
    static constexpr std::string_view NAME = "RenamePipe";
};

struct RetirePipe final : public Pipe {
    static constexpr std::tuple FIELDS = {};
    static constexpr std::string_view NAME = "RetirePipe";
};

struct SimpleExecutionPipe final : public Pipe {
    std::size_t delay;
    std::optional<std::unordered_set<Instruction::Kind>> filter_by;
    static constexpr std::tuple FIELDS = {
        Field{&SimpleExecutionPipe::delay, "delay"},
        Field{&SimpleExecutionPipe::filter_by, "filter_by"},
    };
    static constexpr std::string_view NAME = "SimpleExecutionPipe";
};

struct SchedulerExecutionPortInfo final {
    Arc<Pipe> pipe;
    std::optional<std::size_t> pipe_count;

    static constexpr std::tuple FIELDS = {
        Field{&SchedulerExecutionPortInfo::pipe, "pipe"},
        Field{&SchedulerExecutionPortInfo::pipe_count, "pipe_count"},
    };
    static constexpr std::string_view NAME = "SchedulerExecutionPortInfo";
};

struct SimpleSchedulerPipe final : public Pipe {
    std::vector<SchedulerExecutionPortInfo> execution_ports;

    static constexpr std::tuple FIELDS = {
        Field{&SimpleSchedulerPipe::execution_ports, "execution_ports"},
    };
    static constexpr std::string_view NAME = "SimpleSchedulerPipe";
};

using PipeVariant = std::variant<FetchPipe, RenamePipe, RetirePipe,
                                 SimpleExecutionPipe, SimpleSchedulerPipe>;

static_assert(Parsable<PipeVariant>);

namespace detail {
template <typename Fn, typename... PipeTypes>
constexpr void pipe_visit_helper(Pipe &pipe, Fn &&fn,
                                 std::variant<PipeTypes...> *) {
    auto helper = [&](auto *pipe) -> bool {
        if (pipe) {
            std::forward<Fn>(fn)(*pipe);
            return true;
        }
        return false;
    };
    bool found = (helper(dynamic_cast<PipeTypes *>(&pipe)) || ...);
    assert(found && "unknown Pipe subclass");
}
} // namespace detail

template <typename Fn>
constexpr void Pipe::visit(Fn &&fn) {
    detail::pipe_visit_helper(*this, std::forward<Fn>(fn),
                              (PipeVariant *)nullptr);
}
} // namespace config

inline config_parser::ParseResult<Arc<config::Pipe>>
config_parser::ParserFor<Arc<config::Pipe>>::parse(
    std::optional<TomlValue> toml, std::string_view field_name) {
    using namespace config;
    auto result = ParserFor<PipeVariant>::parse(toml, field_name);
    if (ParseFailure *e = std::get_if<ParseFailure>(&result))
        return std::move(*e);
    return std::visit(
        [](auto pipe) -> Arc<Pipe> {
            return make_arc<decltype(pipe)>(std::move(pipe));
        },
        std::get<PipeVariant>(std::move(result)));
}

namespace config {
struct TopLevelConfig final {
    std::vector<Arc<Pipe>> stages;
    std::uint64_t simulation_cycles;

    static constexpr std::tuple FIELDS = {
        Field{&TopLevelConfig::stages, "stages"},
        Field{&TopLevelConfig::simulation_cycles, "simulation_cycles"},
    };
    static constexpr std::string_view NAME = "TopLevelConfig";
};

static_assert(Parsable<TopLevelConfig>);
} // namespace config
