// SPDX-License-Identifier: LGPL-2.1-or-later
// See Notices.txt for copyright information
#pragma once

#include "arc.h"
#include "c_api.h"
#include "instruction.h"
#include "instruction_state.h"
#include "simulator.h"
#include <concepts>
#include <functional>
#include <tuple>
#include <type_traits>

struct PipeInstructionsRef final {
    std::span<const std::shared_ptr<InstructionState>> instructions;
    Arc<const void> pipe_ref;
    static PipeInstructionsRef empty() noexcept {
        return {.instructions{}, .pipe_ref{}};
    }
};

class PipeFactory;

class Pipe : public simulator::Simulatable, public EnableArcFromThis<Pipe> {
private:
    Arc<Pipe> m_input_pipe;
    Arc<const PipeFactory> m_factory;

public:
    Pipe(Arc<Pipe> input_pipe, Arc<const PipeFactory> factory,
         simulator::Simulator &simulator) noexcept
        : m_input_pipe(std::move(input_pipe)), m_factory(std::move(factory)) {
        (void)simulator;
    }
    PipeInstructionsRef pipe_input_instructions() const noexcept {
        if (m_input_pipe)
            return m_input_pipe->pipe_output_instructions();
        console_log("WARNING: m_input_pipe not set");
        return PipeInstructionsRef::empty();
    }
    const Arc<const PipeFactory> &factory() const noexcept {
        return m_factory;
    }
    virtual PipeInstructionsRef pipe_output_instructions() const noexcept = 0;
};

template <typename T, typename... ExtraArgs>
concept BuildablePipe = (... && std::movable<ExtraArgs>)&&requires(
    Arc<Pipe> input_pipe, Arc<PipeFactory> factory,
    simulator::Simulator &simulator, const ExtraArgs &...extra_args) {
    {
        new T(std::move(input_pipe), std::move(factory), simulator,
              extra_args...)
        } -> std::convertible_to<Pipe *>;
};

template <typename ThePipe, typename TheInstruction, typename... ExtraArgs>
class PipeFactoryFor;

class PipeFactory {
public:
    constexpr virtual ~PipeFactory() = default;
    constexpr virtual bool
    is_instruction_handled(const Instruction &instruction) const noexcept = 0;
    virtual Arc<Pipe> create(Arc<Pipe> input_pipe,
                             simulator::Simulator &simulator) const = 0;

    template <typename ThePipe, std::derived_from<Instruction> TheInstruction,
              typename... ExtraArgs>
    static Arc<PipeFactoryFor<ThePipe, TheInstruction, ExtraArgs...>>
    make_factory_for(ExtraArgs... extra_args) noexcept requires
        BuildablePipe<ThePipe, ExtraArgs...>;
};

template <typename... ExtraArgs, BuildablePipe<ExtraArgs...> ThePipe,
          std::derived_from<Instruction> TheInstruction>
/// The PipeFactory for ThePipe
class PipeFactoryFor<ThePipe, TheInstruction, ExtraArgs...> final
    : public PipeFactory,
      public EnableArcFromThis<
          PipeFactoryFor<ThePipe, TheInstruction, ExtraArgs...>> {
private:
    std::tuple<ExtraArgs...> m_extra_args;

public:
    explicit PipeFactoryFor(ExtraArgs &&...extra_args) noexcept
        : m_extra_args{std::move(extra_args)...} {}
    constexpr bool is_instruction_handled(
        const Instruction &instruction) const noexcept final {
        return (bool)dynamic_cast<const TheInstruction *>(&instruction);
    }
    Arc<Pipe> create(Arc<Pipe> input_pipe,
                     simulator::Simulator &simulator) const final {
        Arc<const PipeFactory> factory = this->arc_from_this();
        auto helper = [&](const ExtraArgs &...extra_args) -> Arc<Pipe> {
            Arc<Pipe> pipe =
                make_arc<ThePipe>(std::move(input_pipe), std::move(factory),
                                  simulator, extra_args...);
            simulator.insert(pipe);
            return pipe;
        };
        return std::apply(helper, m_extra_args);
    }
};

template <typename ThePipe, std::derived_from<Instruction> TheInstruction,
          typename... ExtraArgs>
Arc<PipeFactoryFor<ThePipe, TheInstruction, ExtraArgs...>>
PipeFactory::make_factory_for(ExtraArgs... extra_args) noexcept requires
    BuildablePipe<ThePipe, ExtraArgs...> {
    return make_arc<PipeFactoryFor<ThePipe, TheInstruction, ExtraArgs...>>(
        std::move(extra_args)...);
}

class InputPipe final : public Pipe {
private:
    simulator::State<std::vector<std::shared_ptr<InstructionState>>>
        m_output_instructions;

public:
    explicit InputPipe(Arc<Pipe> input_pipe, Arc<const PipeFactory> factory,
                       simulator::Simulator &simulator,
                       std::vector<std::shared_ptr<InstructionState>>
                           initial_output_instructions) noexcept
        : Pipe(nullptr, std::move(factory), simulator),
          m_output_instructions(simulator,
                                std::move(initial_output_instructions)) {
        assert(!input_pipe);
    }
    static Arc<const PipeFactory>
    make_factory(std::vector<std::shared_ptr<InstructionState>>
                     initial_output_instructions = {}) noexcept {
        return PipeFactory::make_factory_for<InputPipe, NoInstruction>(
            std::move(initial_output_instructions));
    }
    simulator::StateChanged
    set_next_output_instructions(std::vector<std::shared_ptr<InstructionState>>
                                     next_output_instructions) noexcept {
        return m_output_instructions.set_next(
            std::move(next_output_instructions));
    }
    PipeInstructionsRef pipe_output_instructions() const noexcept final {
        return {.instructions = m_output_instructions.current(),
                .pipe_ref = arc_from_this()};
    }
    simulator::StateChanged
    compute_next_state(simulator::Simulator &) noexcept final {
        return simulator::StateChanged::Unchanged;
    }
};
