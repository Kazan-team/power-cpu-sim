// SPDX-License-Identifier: LGPL-2.1-or-later
// See Notices.txt for copyright information
#pragma once

#include "arc.h"
#include "c_api.h"
#include "instruction_state.h"
#include "pipe.h"
#include "register.h"
#include "scheduler.h"
#include "simulator.h"
#include <memory>
#include <optional>
#include <sstream>
#include <unordered_map>
#include <unordered_set>
#include <variant>
#include <vector>

class SimpleSchedulerPipe final : public SchedulerPipe {
private:
    struct RegisterState final {
        std::vector<std::shared_ptr<InstructionState>> waiting_instructions;
        bool ready;
        bool operator==(const RegisterState &) const noexcept = default;
    };
    Arc<RegisterState> m_ready_register_state =
        make_arc<RegisterState>(RegisterState{
            .waiting_instructions{},
            .ready = true,
        });
    struct SchedulerInstructionState final {
        std::shared_ptr<InstructionState> instruction;
        std::vector<Arc<RegisterState>> input_registers;
        std::vector<Arc<RegisterState>> output_registers;
        bool finished;
        bool
        operator==(const SchedulerInstructionState &) const noexcept = default;
    };
    struct State final {
        std::vector<std::shared_ptr<InstructionState>> output_instructions;
        std::vector<SchedulerInstructionState> waiting_instruction_states;
        std::unordered_map<std::shared_ptr<InstructionState>,
                           SchedulerInstructionState>
            executing_instruction_states;
        std::vector<std::shared_ptr<InstructionState>>
            executing_instructions_queue;
        std::unordered_map<Register, Arc<RegisterState>> register_states;
        bool operator==(const State &) const noexcept = default;
    };
    simulator::State<State> m_state;

public:
    explicit SimpleSchedulerPipe(
        Arc<Pipe> input_pipe, Arc<const PipeFactory> factory,
        simulator::Simulator &simulator,
        std::span<const ExecutionPortInfo> execution_port_infos)
        : SchedulerPipe(std::move(input_pipe), std::move(factory), simulator,
                        execution_port_infos),
          m_state(simulator) {}
    static Arc<const PipeFactory>
    make_factory(std::vector<ExecutionPortInfo> execution_port_infos) {
        // vector instead of span to avoid argument disappearing
        return PipeFactory::make_factory_for<SimpleSchedulerPipe, Instruction>(
            std::move(execution_port_infos));
    }
    PipeInstructionsRef pipe_output_instructions() const noexcept final {
        return {.instructions = m_state.current().output_instructions,
                .pipe_ref = arc_from_this()};
    }
    simulator::StateChanged
    compute_next_state(simulator::Simulator &simulator) noexcept final {
        State state = m_state.current();
        auto input_instructions = pipe_input_instructions();
        for (auto &input_instruction : input_instructions.instructions) {
            std::vector<Arc<RegisterState>> input_registers;
            input_registers.reserve(
                input_instruction->instruction.current()->inputs().size());
            for (auto input_reg :
                 input_instruction->instruction.current()->inputs()) {
                // get the register state, inserting m_ready_register_state if
                // the register was not ever seen before since we can then treat
                // it as a pre-initialized register or something
                auto &register_state =
                    state.register_states
                        .emplace(input_reg, m_ready_register_state)
                        .first->second;

                if (!register_state->ready)
                    register_state->waiting_instructions.push_back(
                        input_instruction);
                input_registers.push_back(register_state);
            }
            std::vector<Arc<RegisterState>> output_registers;
            output_registers.reserve(
                input_instruction->instruction.current()->outputs().size());
            for (auto output_reg :
                 input_instruction->instruction.current()->outputs()) {
                auto register_state = make_arc<RegisterState>(RegisterState{
                    .waiting_instructions{},
                    .ready = false,
                });
                state.register_states.insert_or_assign(output_reg,
                                                       register_state);
                output_registers.push_back(std::move(register_state));
            }
            state.waiting_instruction_states.push_back(
                SchedulerInstructionState{
                    .instruction = input_instruction,
                    .input_registers = std::move(input_registers),
                    .output_registers = std::move(output_registers),
                    .finished = false,
                });
        }
        auto changed = simulator::StateChanged::Unchanged;
        state.output_instructions.clear();
        for (auto &execution_port : execution_ports()) {
            for (auto &pipe : execution_port.pipes()) {
                auto pipe_output_instructions =
                    pipe.pipe->pipe_output_instructions();
                for (auto &instruction :
                     pipe_output_instructions.instructions) {
                    auto instruction_state_iter =
                        state.executing_instruction_states.find(instruction);
                    if (instruction_state_iter ==
                        state.executing_instruction_states.end()) {
                        std::ostringstream ss;
                        ss << "Unknown instruction finished execution: "
                           << *instruction->instruction.current();
                        changed |= instruction->state.set_next(ss.str());
                        console_log(std::move(ss).str());
                        continue;
                    }
                    instruction_state_iter->second.finished = true;
                }
            }
        }
        std::vector<std::shared_ptr<InstructionState>>
            old_executing_instructions_queue;
        old_executing_instructions_queue.swap(
            state.executing_instructions_queue);
        for (auto &instruction : old_executing_instructions_queue) {
            auto instruction_state_iter =
                state.executing_instruction_states.find(instruction);
            assert(instruction_state_iter !=
                   state.executing_instruction_states.end());
            auto &scheduler_instruction_state = instruction_state_iter->second;
            if (!scheduler_instruction_state.finished) {
                state.executing_instructions_queue.push_back(
                    std::move(instruction));
                continue;
            }
            std::ostringstream ss;
            ss << "Write Outputs:";
            auto separator = " ";
            std::span<const Register> output_regs =
                instruction->instruction.current()->outputs();
            for (std::size_t i = 0; i < output_regs.size(); i++) {
                auto &output_reg = output_regs[i];
                auto &output =
                    scheduler_instruction_state.output_registers.at(i);
                output->ready = true;
                ss << separator << output_reg.name();
                separator = ", ";
            }
            state.executing_instruction_states.erase(instruction_state_iter);
            console_log(instruction->instruction.current()->to_string() + ": " +
                        ss.str());
            changed |= instruction->state.set_next(std::move(ss).str());
            state.output_instructions.push_back(instruction);
        }
        std::unordered_set<const InputPipe *> filled_pipes;
        std::vector<SchedulerInstructionState> old_waiting_instruction_states;
        old_waiting_instruction_states.swap(state.waiting_instruction_states);
        for (auto &scheduler_instruction_state :
             old_waiting_instruction_states) {
            auto &instruction = scheduler_instruction_state.instruction;
            {
                bool all_inputs_ready = true;
                std::span<const Register> input_regs =
                    instruction->instruction.current()->inputs();
                std::ostringstream ss;
                ss << "Wait:";
                auto separator = " ";
                for (std::size_t i = 0; i < input_regs.size(); i++) {
                    auto &input_reg = input_regs[i];
                    auto &input =
                        scheduler_instruction_state.input_registers.at(i);
                    if (!input->ready) {
                        all_inputs_ready = false;
                        ss << separator << input_reg.name();
                        separator = ", ";
                    }
                }
                if (!all_inputs_ready) {
                    changed |= instruction->state.set_next(ss.str());
                    state.waiting_instruction_states.push_back(
                        std::move(scheduler_instruction_state));
                    continue;
                }
            }
            bool any_execution_ports_found = false;
            bool started = false;
            for (auto &execution_port : execution_ports()) {
                if (!execution_port.info()
                         .execution_pipe_factory->is_instruction_handled(
                             *instruction->instruction.current()))
                    continue;
                any_execution_ports_found = true;
                auto try_fill_pipe = [&](const ExecutionPort::PipeData &pipe) {
                    if (!filled_pipes.insert(&*pipe.input_pipe).second)
                        return false;
                    changed |= pipe.input_pipe->set_next_output_instructions(
                        {instruction});
                    started = true;
                    return true;
                };
                for (auto &pipe : execution_port.pipes()) {
                    if (try_fill_pipe(pipe))
                        break;
                }
                if (started)
                    break;
                if (execution_port.try_append_new_pipe(simulator)) {
                    bool filled = try_fill_pipe(execution_port.pipes().back());
                    assert(filled);
                }
            }
            if (!any_execution_ports_found) {
                std::ostringstream ss;
                ss << "Instruction permanently stuck with no execution ports "
                      "that handle it: "
                   << *instruction->instruction.current();
                auto msg = std::move(ss).str();
                console_log(msg);
                changed |= instruction->state.set_next(std::move(msg));
                state.waiting_instruction_states.push_back(
                    std::move(scheduler_instruction_state));
                continue;
            }
            if (started) {
                std::ostringstream ss;
                ss << "Read Inputs:";
                auto separator = " ";
                for (auto &input_reg :
                     instruction->instruction.current()->inputs()) {
                    ss << separator << input_reg.name();
                    separator = ", ";
                }
                changed |= instruction->state.set_next(std::move(ss).str());
                auto instruction_copy = scheduler_instruction_state.instruction;
                state.executing_instruction_states.emplace(
                    instruction_copy, std::move(scheduler_instruction_state));
                state.executing_instructions_queue.push_back(
                    std::move(instruction_copy));
                continue;
            }
            changed |=
                instruction->state.set_next("Wait: all execution pipes busy");
            state.waiting_instruction_states.push_back(
                std::move(scheduler_instruction_state));
        }
        for (auto &execution_port : execution_ports()) {
            for (auto &pipe : execution_port.pipes()) {
                if (!filled_pipes.contains(&*pipe.input_pipe)) {
                    pipe.input_pipe->set_next_output_instructions({});
                }
            }
        }
        changed |= m_state.set_next(std::move(state));
        return changed;
    }
};