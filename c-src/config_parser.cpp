// SPDX-License-Identifier: LGPL-2.1-or-later
// See Notices.txt for copyright information
#include "config_parser.h"
#include <cstdlib>
#include <iostream>

[[noreturn]] void config_parser::ParseFailure::print_and_exit() const {
    std::cerr << "Error parsing config TOML: " << to_string() << std::endl;
    std::exit(1);
}