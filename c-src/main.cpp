// SPDX-License-Identifier: LGPL-2.1-or-later
// See Notices.txt for copyright information
#include "arc.h"
#include "c_api.h"
#include "config.h"
#include "config_parser.h"
#include "fetch_pipe.h"
#include "final_pipe.h"
#include "instruction_state.h"
#include "instruction_trace.h"
#include "instructions.h"
#include "pipe.h"
#include "register.h"
#include "rename_pipe.h"
#include "retire_pipe.h"
#include "simple_execution_pipe.h"
#include "simple_scheduler.h"
#include "simulator.h"
#include "tracking_table_writer.h"
#include <cstdint>
#include <functional>
#include <ios>
#include <memory>
#include <optional>
#include <span>
#include <sstream>
#include <string>
#include <string_view>
#include <utility>
#include <variant>
#include <vector>

static Arc<const InstructionTrace>
make_instruction_trace(simulator::Simulator &simulator,
                       TrackingTableWriterBuilder &table_writer_builder) {
    InstructionTrace::Builder builder;
    InstructionAddress pc = 0x100;
    auto add = [&](auto instruction) {
        auto next_pc = pc + instruction.size_in_bytes();
        builder.add(pc, std::move(instruction));
        pc = next_pc;
    };
#if 1
    for (std::size_t i = 0; i < 20; i++) {
        auto loop_start = pc;
        add(instructions::Addi(ISARegs::R3, ISARegs::R3, -1));
        add(instructions::Cmpdi(ISARegs::CR0, ISARegs::R3, 0));
        add(instructions::Bne<std::string_view>(".L2"));
        pc = loop_start;
    }
#else
    add(instructions::Mtctr(ISARegs::R4));
    for (std::size_t i = 0; i < 6; i++) {
        auto loop_start = pc;
        add(instructions::Ldu(ISARegs::R9, 8, ISARegs::R3));
        add(instructions::Addi(ISARegs::R9, ISARegs::R9, 100));
        add(instructions::Std(ISARegs::R9, 0, ISARegs::R3));
        add(instructions::Bdnz<std::string_view>(".L2"));
        pc = loop_start;
    }
#endif
    return builder.build(simulator, table_writer_builder);
}

struct PipeBuilder final {
    simulator::Simulator &simulator;
    Arc<FetchPipe> fetch_pipe;
    Arc<RetireInputPipe> retire_input_pipe;
    Arc<RenamePipe> rename_pipe;
    Arc<Pipe> last_pipe;
    Arc<const InstructionTrace> instruction_trace;
    explicit PipeBuilder(simulator::Simulator &simulator,
                         Arc<const InstructionTrace> instruction_trace) noexcept
        : simulator(simulator), fetch_pipe(), retire_input_pipe(),
          rename_pipe(),
          last_pipe(InputPipe::make_factory()->create(nullptr, simulator)),
          instruction_trace(instruction_trace) {
        // just so there's something when there isn't a fetch pipe
        retire_input_pipe = static_arc_cast<RetireInputPipe>(
            RetireInputPipe::make_factory()->create(last_pipe, simulator));
    }
    std::optional<config_parser::ParseFailure>
    build(std::span<const Arc<config::Pipe>> stages) && {
        for (auto &pipe : stages) {
            std::optional<config_parser::ParseFailure> e;
            pipe->visit([&](auto &pipe) { e = visit_pipe(pipe); });
            if (e)
                return e;
        }
        auto final_pipe = static_arc_cast<FinalPipe>(
            FinalPipe::make_factory()->create(last_pipe, simulator));
        if (rename_pipe) {
            std::weak_ptr<FinalPipe> final_pipe_weak(
                static_cast<std::shared_ptr<FinalPipe>>(final_pipe));
            rename_pipe->set_get_freed_registers([final_pipe_weak] {
                return final_pipe_weak.lock()->freed_registers();
            });
        }
        return std::nullopt;
    }

private:
    std::optional<config_parser::ParseFailure>
    visit_pipe(const config::FetchPipe &cfg) {
        if (fetch_pipe)
            return config_parser::ParseFailure(
                "can't have multiple fetch pipes");
        fetch_pipe = static_arc_cast<FetchPipe>(
            FetchPipe::make_factory(cfg, instruction_trace)
                ->create(last_pipe, simulator));
        retire_input_pipe = static_arc_cast<RetireInputPipe>(
            RetireInputPipe::make_factory()->create(fetch_pipe, simulator));
        last_pipe = retire_input_pipe;
        return std::nullopt;
    }
    std::optional<config_parser::ParseFailure>
    visit_pipe(const config::RenamePipe &cfg) {
        if (rename_pipe)
            return config_parser::ParseFailure(
                "can't have multiple rename pipes");
        rename_pipe =
            static_arc_cast<RenamePipe>(RenamePipe::make_factory(nullptr, cfg)
                                            ->create(last_pipe, simulator));
        last_pipe = rename_pipe;
        return std::nullopt;
    }
    struct ExecutionPipeFactoryMaker final {
        PipeBuilder &builder;
        config_parser::ParseResult<Arc<const PipeFactory>>
        visit_pipe(const config::SimpleExecutionPipe &cfg) {
            return SimpleExecutionPipe::make_factory(cfg);
        }
        template <typename T>
        config_parser::ParseFailure fail(const T &) {
            return config_parser::ParseFailure(
                std::string(T::NAME) + " is not supported inside a scheduler");
        }
        config_parser::ParseResult<Arc<const PipeFactory>>
        visit_pipe(const config::SimpleSchedulerPipe &cfg) {
            return fail(cfg);
        }
        config_parser::ParseResult<Arc<const PipeFactory>>
        visit_pipe(const config::RetirePipe &cfg) {
            return fail(cfg);
        }
        config_parser::ParseResult<Arc<const PipeFactory>>
        visit_pipe(const config::RenamePipe &cfg) {
            return fail(cfg);
        }
        config_parser::ParseResult<Arc<const PipeFactory>>
        visit_pipe(const config::FetchPipe &cfg) {
            return fail(cfg);
        }
    };
    config_parser::ParseResult<std::vector<SchedulerPipe::ExecutionPortInfo>>
    parse_execution_port_infos(
        std::span<const config::SchedulerExecutionPortInfo>
            execution_port_infos_in) {
        std::vector<SchedulerPipe::ExecutionPortInfo> execution_port_infos;
        for (auto &execution_port : execution_port_infos_in) {
            config_parser::ParseResult<Arc<const PipeFactory>> parse_result;
            execution_port.pipe->visit([&](auto &pipe) {
                ExecutionPipeFactoryMaker v{
                    .builder = *this,
                };
                parse_result = v.visit_pipe(pipe);
            });
            if (auto *e =
                    std::get_if<config_parser::ParseFailure>(&parse_result))
                return std::move(*e);
            execution_port_infos.push_back(SchedulerPipe::ExecutionPortInfo{
                .execution_pipe_factory =
                    std::get<Arc<const PipeFactory>>(std::move(parse_result)),
                .pipe_count = execution_port.pipe_count,
            });
        }
        return std::move(execution_port_infos);
    }
    std::optional<config_parser::ParseFailure>
    visit_pipe(const config::SimpleSchedulerPipe &cfg) {
        auto ports = parse_execution_port_infos(cfg.execution_ports);
        if (auto *e = std::get_if<config_parser::ParseFailure>(&ports))
            return std::move(*e);
        last_pipe = SimpleSchedulerPipe::make_factory(
                        std::get<std::vector<SchedulerPipe::ExecutionPortInfo>>(
                            std::move(ports)))
                        ->create(last_pipe, simulator);
        return std::nullopt;
    }
    std::optional<config_parser::ParseFailure>
    visit_pipe(const config::RetirePipe &cfg) {
        last_pipe = RetirePipe::make_factory(retire_input_pipe, cfg)
                        ->create(last_pipe, simulator);
        return std::nullopt;
    }
    std::optional<config_parser::ParseFailure>
    visit_pipe(const config::SimpleExecutionPipe &cfg) {
        last_pipe = SimpleExecutionPipe::make_factory(cfg)->create(last_pipe,
                                                                   simulator);
        return std::nullopt;
    }
};

static void main_impl(Output output, TomlValue config_toml) noexcept {
    auto cfg =
        config_parser::parse_or_exit<config::TopLevelConfig>(config_toml);
    TrackingTableWriterBuilder table_writer_builder(std::move(output));
    auto cycle_counter = table_writer_builder.add_column("Cycle");
    simulator::Simulator simulator;
    auto instruction_trace =
        make_instruction_trace(simulator, table_writer_builder);
    if (auto e = PipeBuilder(simulator, instruction_trace).build(cfg.stages)) {
        e->print_and_exit();
    }
    auto table_writer = std::move(table_writer_builder).build();
    for (std::uint64_t cycle = 0; cycle < cfg.simulation_cycles; cycle++) {
        if (cycle != 0)
            simulator.step();
        cycle_counter->set_value(std::to_string(cycle));
        table_writer.write_row();
    }
    output = table_writer.finish();
}

void c_main(OutputFFI *output, const TableWriterVTable *vtable,
            const TomlValueFFI *config) {
    main_impl(Output(output, vtable), TomlValue(config));
}