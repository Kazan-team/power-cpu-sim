use crate::table::TableWriter;
use std::{cell::Cell, ffi::CStr, marker::PhantomData, os::raw::c_char, ptr, slice, str};

// declarations *must* match those in ../src/c_api.rs
// due to ABI mismatch issues, all structs must be passed by pointer or manually decomposed into
// scalar values

#[cfg(target_arch = "wasm32")]
/// cbindgen:ignore
extern "C" {
    fn js_console_log(text: *const u8, text_len: usize);
    fn set_output_table(text: *const u8, text_len: usize);
}

pub fn console_log_impl(text: &str) {
    #[cfg(target_arch = "wasm32")]
    unsafe {
        js_console_log(text.as_ptr(), text.len())
    };
    #[cfg(not(target_arch = "wasm32"))]
    eprintln!("{}", text);
}

pub fn write_output(text: &str, is_html: bool) {
    #[cfg(target_arch = "wasm32")]
    unsafe {
        if is_html {
            set_output_table(text.as_ptr(), text.len());
            return;
        }
    }
    let _ = is_html;
    println!("{}", text);
}

fn bytes_to_str(bytes: &[u8]) -> &str {
    str::from_utf8(bytes).expect("invalid UTF-8")
}

unsafe fn cstr_to_str<'a>(utf8_text: *const c_char) -> &'a str {
    assert!(!utf8_text.is_null());
    bytes_to_str(CStr::from_ptr(utf8_text).to_bytes())
}

#[no_mangle]
pub unsafe extern "C" fn console_log(utf8_text: *const u8, len: usize) {
    console_log_impl(
        StrFFI {
            utf8_text,
            len,
            _phantom: PhantomData,
        }
        .to_str(),
    );
}

#[no_mangle]
pub unsafe extern "C" fn console_log_cstr(utf8_text: *const c_char) {
    console_log_impl(cstr_to_str(utf8_text));
}

#[derive(Copy, Clone)]
#[repr(C)]
pub struct StrFFI<'a> {
    pub utf8_text: *const u8,
    pub len: usize,
    pub _phantom: PhantomData<&'a str>,
}

impl<'a> StrFFI<'a> {
    unsafe fn to_str(self) -> &'a str {
        let Self {
            utf8_text,
            len,
            _phantom,
        } = self;
        bytes_to_str(
            ConstSliceFFI {
                data: utf8_text,
                len,
                _phantom: PhantomData,
            }
            .to_slice(),
        )
    }
}

#[derive(Copy, Clone)]
#[repr(C)]
pub struct ConstSliceFFI<'a, T> {
    pub data: *const T,
    pub len: usize,
    pub _phantom: PhantomData<&'a [T]>,
}

impl<'a, T> ConstSliceFFI<'a, T> {
    unsafe fn to_slice(self) -> &'a [T] {
        if self.len == 0 {
            return &[];
        }
        assert!(!self.data.is_null());
        slice::from_raw_parts(self.data, self.len)
    }
    unsafe fn cast<'b, U>(self) -> ConstSliceFFI<'b, U> {
        ConstSliceFFI {
            data: self.data as *const U,
            len: self.len,
            _phantom: PhantomData,
        }
    }
}

#[repr(transparent)]
struct UncheckedStrFFI<'a>(StrFFI<'a>);

impl AsRef<str> for UncheckedStrFFI<'_> {
    fn as_ref(&self) -> &str {
        unsafe { self.0.to_str() }
    }
}

#[repr(C)]
pub struct TableWriterVTable {
    pub output_drop: unsafe extern "C" fn(output: *mut OutputFFI),
    pub table_writer_drop: unsafe extern "C" fn(table_writer: *mut TableWriterFFI),
    pub start: unsafe extern "C" fn(
        output: *mut OutputFFI,
        header_columns_data: *const StrFFI,
        header_columns_len: usize,
    ) -> *mut TableWriterFFI,
    pub write_row: unsafe extern "C" fn(
        table_writer: *mut TableWriterFFI,
        columns_data: *const StrFFI,
        columns_len: usize,
    ),
    pub finish: unsafe extern "C" fn(table_writer: *mut TableWriterFFI) -> *mut OutputFFI,
    pub write_plain_text:
        unsafe extern "C" fn(output: *mut OutputFFI, utf8_text: *const u8, len: usize),
}

pub struct OutputFFI {
    _private: [u8; 0],
}

impl OutputFFI {
    fn into_ffi<T: TableWriter>(v: Box<T::Output>) -> *mut OutputFFI {
        Box::into_raw(v) as *mut OutputFFI
    }
    unsafe fn as_mut<'a, T: TableWriter>(ptr: *mut OutputFFI) -> &'a mut T::Output {
        (ptr as *mut T::Output).as_mut().unwrap()
    }
    unsafe fn from_ffi<T: TableWriter>(ptr: *mut OutputFFI) -> Box<T::Output> {
        assert!(!ptr.is_null());
        Box::from_raw(ptr as *mut T::Output)
    }
    pub fn from_output<T: TableWriter>(
        output: T::Output,
    ) -> (*mut OutputFFI, *const TableWriterVTable) {
        unsafe extern "C" fn output_drop<T: TableWriter>(ptr: *mut OutputFFI) {
            OutputFFI::from_ffi::<T>(ptr);
        }
        unsafe extern "C" fn table_writer_drop<T: TableWriter>(ptr: *mut TableWriterFFI) {
            TableWriterFFI::from_ffi::<T>(ptr);
        }
        unsafe extern "C" fn start<T: TableWriter>(
            output: *mut OutputFFI,
            header_columns_data: *const StrFFI,
            header_columns_len: usize,
        ) -> *mut TableWriterFFI {
            let output = *OutputFFI::from_ffi::<T>(output);
            let table_writer = T::start(
                output,
                ConstSliceFFI {
                    data: header_columns_data,
                    len: header_columns_len,
                    _phantom: PhantomData,
                }
                .cast::<UncheckedStrFFI>()
                .to_slice(),
            )
            .unwrap();
            TableWriterFFI::into_ffi::<T>(Box::new(table_writer))
        }
        unsafe extern "C" fn write_row<T: TableWriter>(
            table_writer: *mut TableWriterFFI,
            columns_data: *const StrFFI,
            columns_len: usize,
        ) {
            let table_writer = TableWriterFFI::as_mut::<T>(table_writer);
            table_writer
                .write_row(
                    ConstSliceFFI {
                        data: columns_data,
                        len: columns_len,
                        _phantom: PhantomData,
                    }
                    .cast::<UncheckedStrFFI>()
                    .to_slice(),
                )
                .unwrap();
        }
        unsafe extern "C" fn finish<T: TableWriter>(
            table_writer: *mut TableWriterFFI,
        ) -> *mut OutputFFI {
            let table_writer = *TableWriterFFI::from_ffi::<T>(table_writer);
            let output = table_writer.finish().unwrap();
            OutputFFI::into_ffi::<T>(Box::new(output))
        }
        unsafe extern "C" fn write_plain_text<T: TableWriter>(
            output: *mut OutputFFI,
            utf8_text: *const u8,
            len: usize,
        ) {
            let output = OutputFFI::as_mut::<T>(output);
            T::write_plain_text(
                output,
                StrFFI {
                    utf8_text,
                    len,
                    _phantom: PhantomData,
                }
                .to_str(),
            )
            .unwrap();
        }
        let vtable: &'static TableWriterVTable = &TableWriterVTable {
            output_drop: output_drop::<T>,
            table_writer_drop: table_writer_drop::<T>,
            start: start::<T>,
            write_row: write_row::<T>,
            finish: finish::<T>,
            write_plain_text: write_plain_text::<T>,
        };
        (OutputFFI::into_ffi::<T>(Box::new(output)), vtable)
    }
}

pub struct TableWriterFFI {
    _private: [u8; 0],
}

impl TableWriterFFI {
    fn into_ffi<T: TableWriter>(v: Box<T>) -> *mut TableWriterFFI {
        Box::into_raw(v) as *mut TableWriterFFI
    }
    unsafe fn as_mut<'a, T: TableWriter>(ptr: *mut TableWriterFFI) -> &'a mut T {
        (ptr as *mut T).as_mut().unwrap()
    }
    unsafe fn from_ffi<T: TableWriter>(ptr: *mut TableWriterFFI) -> Box<T> {
        assert!(!ptr.is_null());
        Box::from_raw(ptr as *mut T)
    }
}

#[derive(Copy, Clone)]
#[repr(C, u8)]
pub enum TomlValueFFI<'a> {
    Integer(i64),
    String(StrFFI<'a>),
    Float(f64),
    Boolean(bool),
    Datetime(StrFFI<'a>),
    Array(ConstSliceFFI<'a, TomlValueFFI<'a>>),
    Table {
        keys: ConstSliceFFI<'a, StrFFI<'a>>,
        values: ConstSliceFFI<'a, TomlValueFFI<'a>>,
    },
}

struct SliceFiller<'a, T> {
    slice: Option<&'a [Cell<T>]>,
    index: usize,
}

impl<'a, T> SliceFiller<'a, T> {
    fn new() -> Self {
        Self {
            slice: None,
            index: 0,
        }
    }
    fn from(slice: &'a [Cell<T>]) -> Self {
        Self {
            slice: Some(slice),
            index: 0,
        }
    }
    fn fill_next(&mut self, value: T) {
        if let Some(slice) = self.slice {
            slice[self.index].set(value);
        }
        self.index += 1;
    }
    fn alloc_subslice(&mut self, len: usize) -> Self {
        let slice = self.slice.map(|slice| &slice[self.index..][..len]);
        self.index += len;
        Self { slice, index: 0 }
    }
    fn slice_ptr(&self) -> *const T {
        self.slice.map_or(ptr::null(), |slice| slice.as_ptr()) as *const T
    }
    fn slice_len(&self) -> usize {
        self.slice.map_or(0, |slice| slice.len())
    }
    fn to_slice_ffi(&self) -> ConstSliceFFI<'a, T> {
        ConstSliceFFI {
            data: self.slice_ptr(),
            len: self.slice_len(),
            _phantom: PhantomData,
        }
    }
}

impl<'a> SliceFiller<'a, u8> {
    fn alloc_text(&mut self, text: &str) -> StrFFI<'a> {
        let mut subslice = self.alloc_subslice(text.len());
        for b in text.bytes() {
            subslice.fill_next(b);
        }
        StrFFI {
            utf8_text: subslice.slice_ptr(),
            len: subslice.slice_len(),
            _phantom: PhantomData,
        }
    }
}

struct Visitor<'a> {
    values: SliceFiller<'a, TomlValueFFI<'a>>,
    bytes: SliceFiller<'a, u8>,
    strings: SliceFiller<'a, StrFFI<'a>>,
}

impl<'a> Visitor<'a> {
    fn visit(&mut self, value: &toml::Value) -> TomlValueFFI<'a> {
        match *value {
            toml::Value::String(ref v) => TomlValueFFI::String(self.bytes.alloc_text(v)),
            toml::Value::Integer(v) => TomlValueFFI::Integer(v),
            toml::Value::Float(v) => TomlValueFFI::Float(v),
            toml::Value::Boolean(v) => TomlValueFFI::Boolean(v),
            toml::Value::Datetime(ref v) => {
                TomlValueFFI::Datetime(self.bytes.alloc_text(&v.to_string()))
            }
            toml::Value::Array(ref array) => {
                let mut subslice = self.values.alloc_subslice(array.len());
                for i in array {
                    subslice.fill_next(self.visit(i));
                }
                TomlValueFFI::Array(subslice.to_slice_ffi())
            }
            toml::Value::Table(ref table) => {
                let mut keys = self.strings.alloc_subslice(table.len());
                let mut values = self.values.alloc_subslice(table.len());
                for (k, v) in table {
                    keys.fill_next(self.bytes.alloc_text(k));
                    values.fill_next(self.visit(v));
                }
                TomlValueFFI::Table {
                    keys: keys.to_slice_ffi(),
                    values: values.to_slice_ffi(),
                }
            }
        }
    }
    fn visit_root(&mut self, value: &toml::Value) {
        let mut subslice = self.values.alloc_subslice(1);
        subslice.fill_next(self.visit(value));
    }
}

pub struct OwnedTomlValueFFI<'a> {
    values: Box<[Cell<TomlValueFFI<'a>>]>,
    bytes: Box<[Cell<u8>]>,
    strings: Box<[Cell<StrFFI<'a>>]>,
}

pub struct OwnedTomlValueFFIBuilder<'a>(OwnedTomlValueFFI<'a>);

impl<'a> OwnedTomlValueFFIBuilder<'a> {
    pub fn new() -> Self {
        Self(OwnedTomlValueFFI {
            values: Box::new([]),
            bytes: Box::new([]),
            strings: Box::new([]),
        })
    }
    pub fn build(&'a mut self, toml_value: &toml::Value) -> &'a OwnedTomlValueFFI<'a> {
        assert!(self.0.values.is_empty(), "already built");
        let mut visitor = Visitor {
            values: SliceFiller::new(),
            bytes: SliceFiller::new(),
            strings: SliceFiller::new(),
        };
        visitor.visit_root(toml_value);
        let empty_value = TomlValueFFI::Integer(0);
        let empty_str = StrFFI {
            utf8_text: ptr::null(),
            len: 0,
            _phantom: PhantomData,
        };
        self.0.values = vec![Cell::new(empty_value); visitor.values.index].into_boxed_slice();
        self.0.bytes = vec![Cell::new(0u8); visitor.bytes.index].into_boxed_slice();
        self.0.strings = vec![Cell::new(empty_str); visitor.strings.index].into_boxed_slice();
        Visitor {
            values: SliceFiller::from(&*self.0.values),
            bytes: SliceFiller::from(&*self.0.bytes),
            strings: SliceFiller::from(&*self.0.strings),
        }
        .visit_root(toml_value);
        &self.0
    }
}

impl<'a> OwnedTomlValueFFI<'a> {
    pub fn as_ptr(&'a self) -> *const TomlValueFFI<'a> {
        assert!(!self.values.is_empty());
        self.values[0].as_ptr()
    }
}

extern "C" {
    #[allow(improper_ctypes)]
    pub fn c_main<'a>(
        output: *mut OutputFFI,
        vtable: *const TableWriterVTable,
        config: *const TomlValueFFI<'a>,
    );
}
