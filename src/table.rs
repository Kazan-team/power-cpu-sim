// SPDX-License-Identifier: LGPL-2.1-or-later
// See Notices.txt for copyright information
use htmlescape::encode_minimal_w;
use std::{
    fmt::Write as _,
    io::{self, BufWriter, Write},
    mem,
    sync::{Arc, Mutex},
};

pub trait TableWriter: Sized {
    type Output: Write;
    fn start(output: Self::Output, header_columns: &[impl AsRef<str>]) -> io::Result<Self>;
    fn write_row(&mut self, columns: &[impl AsRef<str>]) -> io::Result<()>;
    fn finish(self) -> io::Result<Self::Output>;
    fn write_plain_text(output: &mut Self::Output, text: impl AsRef<str>) -> io::Result<()>;
}

pub struct TransposedTableWriter<T: TableWriter> {
    output: T::Output,
    columns: Vec<Vec<String>>,
}

impl<T: TableWriter> TableWriter for TransposedTableWriter<T> {
    type Output = T::Output;

    fn start(output: Self::Output, header_columns: &[impl AsRef<str>]) -> io::Result<Self> {
        let columns = header_columns
            .iter()
            .map(|column| vec![String::from(column.as_ref())])
            .collect();
        Ok(Self { output, columns })
    }

    fn write_row(&mut self, columns: &[impl AsRef<str>]) -> io::Result<()> {
        assert_eq!(columns.len(), self.columns.len());
        for (row, v) in self.columns.iter_mut().zip(columns) {
            row.push(v.as_ref().into());
        }
        Ok(())
    }

    fn finish(self) -> io::Result<Self::Output> {
        let Self { output, columns } = self;
        let mut columns = columns.into_iter();
        let header_columns = columns.next().unwrap_or_default();
        let mut table_writer = T::start(output, &header_columns)?;
        for column in columns {
            table_writer.write_row(&column)?;
        }
        table_writer.finish()
    }

    fn write_plain_text(output: &mut Self::Output, text: impl AsRef<str>) -> io::Result<()> {
        T::write_plain_text(output, text)
    }
}

pub struct NoOpTableWriter {
    output: io::Sink,
    column_count: usize,
}

impl TableWriter for NoOpTableWriter {
    type Output = io::Sink;
    fn start(output: Self::Output, header_columns: &[impl AsRef<str>]) -> io::Result<Self> {
        Ok(Self {
            output,
            column_count: header_columns.len(),
        })
    }

    fn write_row(&mut self, columns: &[impl AsRef<str>]) -> io::Result<()> {
        assert_eq!(columns.len(), self.column_count);
        Ok(())
    }

    fn finish(self) -> io::Result<Self::Output> {
        Ok(self.output)
    }

    fn write_plain_text(_output: &mut Self::Output, _text: impl AsRef<str>) -> io::Result<()> {
        Ok(())
    }
}

pub struct HtmlTableWriter<W: Write> {
    output: W,
    column_count: usize,
}

impl<W: Write> TableWriter for HtmlTableWriter<W> {
    type Output = W;
    fn start(mut output: Self::Output, header_columns: &[impl AsRef<str>]) -> io::Result<Self> {
        let column_count = header_columns.len();
        writeln!(output, "<table><thead><tr>")?;
        for column in header_columns {
            write!(output, "<th>")?;
            encode_minimal_w(column.as_ref(), &mut output)?;
            writeln!(output, "</th>")?;
        }
        writeln!(output, "</tr></thead><tbody>")?;
        Ok(Self {
            output,
            column_count,
        })
    }
    fn write_row(&mut self, columns: &[impl AsRef<str>]) -> io::Result<()> {
        assert_eq!(self.column_count, columns.len());
        writeln!(self.output, "<tr>")?;
        for column in columns {
            write!(self.output, "<td>")?;
            encode_minimal_w(column.as_ref(), &mut self.output)?;
            writeln!(self.output, "</td>")?;
        }
        writeln!(self.output, "</tr>")
    }
    fn finish(mut self) -> io::Result<Self::Output> {
        write!(self.output, "</tbody></table>")?;
        Ok(self.output)
    }
    fn write_plain_text(output: &mut Self::Output, text: impl AsRef<str>) -> io::Result<()> {
        encode_minimal_w(text.as_ref(), output)
    }
}

pub struct MarkdownTableWriter<W: Write> {
    output: W,
    column_count: usize,
    any_rows: bool,
}

fn write_markdown_escaped_str(output: &mut impl Write, text: &str) -> io::Result<()> {
    for ch in text.chars() {
        match ch {
            '<' => write!(output, "&lt;")?,
            '&' => write!(output, "&amp;")?,
            '"' => write!(output, "&quot;")?,
            '\n' => write!(output, "<br/>")?,
            // special escaping to replacement character
            '\0' => write!(output, "\u{fffd}")?,
            // basically everything is special in some markdown variant in some scenario
            '\\' | '`' | '*' | '{' | '}' | '[' | ']' | '(' | ')' | '#' | '+' | '-' | '.' | '!'
            | '_' | '>' => write!(output, "\\{}", ch)?,
            // really, this is the only safe set
            'a'..='z' | 'A'..='Z' | '0'..='9' | ' ' | '\t' => write!(output, "{}", ch)?,
            _ => write!(output, "&#{};", ch as u32)?,
        }
    }
    Ok(())
}

impl<W: Write> TableWriter for MarkdownTableWriter<W> {
    type Output = W;
    fn start(mut output: Self::Output, header_columns: &[impl AsRef<str>]) -> io::Result<Self> {
        let column_count = header_columns.len();
        for column in header_columns {
            write!(output, "|")?;
            write_markdown_escaped_str(&mut output, column.as_ref())?;
        }
        if column_count == 0 {
            writeln!(output, "|&#32;")?; // write a space using a HTML entity so the markdown parser thinks there's some title
        }
        writeln!(output, "|")?;
        for _ in 0..column_count.max(1) {
            write!(output, "|-")?;
        }
        writeln!(output, "|")?;
        Ok(Self {
            output,
            column_count,
            any_rows: false,
        })
    }
    fn write_row(&mut self, columns: &[impl AsRef<str>]) -> io::Result<()> {
        assert_eq!(self.column_count, columns.len());
        for column in columns {
            write!(self.output, "|")?;
            write_markdown_escaped_str(&mut self.output, column.as_ref())?;
        }
        if self.column_count == 0 {
            write!(self.output, "| ")?;
        }
        self.any_rows = true;
        writeln!(self.output, "|")
    }
    fn finish(mut self) -> io::Result<Self::Output> {
        if !self.any_rows {
            for _ in 0..self.column_count.max(1) {
                write!(self.output, "| ")?;
            }
            writeln!(self.output, "|")?;
        }
        Ok(self.output)
    }
    fn write_plain_text(output: &mut Self::Output, text: impl AsRef<str>) -> io::Result<()> {
        write_markdown_escaped_str(output, text.as_ref())
    }
}

fn vcd_escape(output: &mut String, text: &str) {
    output.clear();
    if text.is_empty() {
        output.push('_');
    }
    for b in text.bytes() {
        match b {
            b'\t' => {
                output.push_str("_t");
                continue;
            }
            b'\n' => {
                output.push_str("_n");
                continue;
            }
            b'\r' => {
                output.push_str("_r");
                continue;
            }
            b' ' => {
                output.push_str("_s");
                continue;
            }
            b'_' => {
                output.push_str("__");
                continue;
            }
            b'\0'..=b'\x1F' | b'\x7F' => {}
            _ => {
                if b.is_ascii() {
                    output.push(b as char);
                    continue;
                }
            }
        }
        write!(output, "_{:02X}", b).unwrap();
    }
}

struct VcdWriterWrapper<W: Write>(Arc<Mutex<Option<W>>>);

impl<W: Write> io::Write for VcdWriterWrapper<W> {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        self.0.lock().unwrap().as_mut().unwrap().write(buf)
    }

    fn flush(&mut self) -> io::Result<()> {
        self.0.lock().unwrap().as_mut().unwrap().flush()
    }
}

pub struct VcdTableWriter<W: Write> {
    inner_writer: Arc<Mutex<Option<W>>>,
    writer: vcd::Writer<BufWriter<VcdWriterWrapper<W>>>,
    last_column_values: Option<Vec<String>>,
    column_ids: Vec<vcd::IdCode>,
    timestamp: u64,
}

impl<W: Write> TableWriter for VcdTableWriter<W> {
    type Output = W;

    fn start(output: Self::Output, header_columns: &[impl AsRef<str>]) -> io::Result<Self> {
        let inner_writer = Arc::new(Mutex::new(Some(output)));
        let mut writer = vcd::Writer::new(BufWriter::new(VcdWriterWrapper(inner_writer.clone())));
        writer.comment("Generated by power-cpu-sim")?;
        writer.timescale(1, vcd::TimescaleUnit::US)?;
        writer.add_module("top")?;
        let mut column_ids = Vec::with_capacity(header_columns.len());
        let mut reference = String::new();
        for header_column in header_columns {
            vcd_escape(&mut reference, header_column.as_ref());
            column_ids.push(writer.add_var(vcd::VarType::String, 1, &reference, None)?);
        }
        writer.upscope()?;
        writer.enddefinitions()?;
        Ok(Self {
            inner_writer,
            writer,
            last_column_values: None,
            column_ids,
            timestamp: 0,
        })
    }

    fn write_row(&mut self, columns: &[impl AsRef<str>]) -> io::Result<()> {
        assert_eq!(columns.len(), self.column_ids.len());
        self.writer.timestamp(self.timestamp)?;
        self.timestamp += 1;
        if let Some(last_column_values) = self.last_column_values.as_mut() {
            assert_eq!(last_column_values.len(), self.column_ids.len());
            let mut escaped_text = String::new();
            for ((column, last_column_value), &id) in
                columns.iter().zip(last_column_values).zip(&self.column_ids)
            {
                vcd_escape(&mut escaped_text, column.as_ref());
                if *last_column_value == escaped_text {
                    continue;
                }
                self.writer.change_string(id, &escaped_text)?;
                mem::swap(&mut escaped_text, last_column_value);
            }
        } else {
            self.writer.begin(vcd::SimulationCommand::Dumpvars)?;
            let mut last_column_values = Vec::with_capacity(self.column_ids.len());
            for (column, &id) in columns.iter().zip(&self.column_ids) {
                let mut escaped_text = String::new();
                vcd_escape(&mut escaped_text, column.as_ref());
                self.writer.change_string(id, &escaped_text)?;
                last_column_values.push(escaped_text);
            }
            self.last_column_values = Some(last_column_values);
            self.writer.end()?;
        }
        Ok(())
    }

    fn finish(self) -> io::Result<Self::Output> {
        let Self {
            inner_writer,
            mut writer,
            last_column_values: _,
            column_ids: _,
            timestamp,
        } = self;
        writer.timestamp(timestamp)?;
        mem::drop(writer);
        Ok(Arc::try_unwrap(inner_writer)
            .ok()
            .unwrap()
            .into_inner()
            .unwrap()
            .unwrap())
    }

    fn write_plain_text(output: &mut Self::Output, text: impl AsRef<str>) -> io::Result<()> {
        let mut escaped_text = String::new();
        vcd_escape(&mut escaped_text, text.as_ref());
        vcd::Writer::new(output).comment(&escaped_text)
    }
}
