// SPDX-License-Identifier: LGPL-2.1-or-later
// See Notices.txt for copyright information
use std::{
    borrow::Cow,
    fs,
    io::{self, prelude::*},
    path::{Path, PathBuf},
    process,
};

use structopt::{
    clap::{self, arg_enum},
    StructOpt,
};
use table::{TableWriter, TransposedTableWriter};

pub mod c_api;
pub mod table;

arg_enum! {
    #[derive(Eq, PartialEq, Debug)]
    pub enum OutputType {
        HTML,
        Markdown,
        VCD,
    }
}

impl Default for OutputType {
    fn default() -> Self {
        if cfg!(target_arch = "wasm32") {
            OutputType::HTML
        } else {
            OutputType::Markdown
        }
    }
}

#[derive(StructOpt)]
/// Run the simulator
pub(crate) struct RunSubcommand {
    #[structopt(long, case_insensitive = true, possible_values = &OutputType::variants())]
    /// The type of output.
    pub(crate) output_type: Option<OutputType>,
    #[structopt(long)]
    /// Reformat the output to make it prettier.
    pub(crate) pretty: bool,
    #[structopt(long)]
    /// Transpose the output, swapping rows with columns.
    pub(crate) transpose: bool,
    #[structopt(long, parse(from_os_str))]
    /// The path to the input config TOML file, `-` can be used to read from stdin
    pub(crate) config: Option<PathBuf>,
}

#[derive(StructOpt)]
/// Display the default config toml
pub(crate) struct DumpDefaultConfigSubcommand {}

#[derive(StructOpt)]
pub(crate) enum Command {
    Run(RunSubcommand),
    DumpDefaultConfig(DumpDefaultConfigSubcommand),
}

#[derive(StructOpt)]
pub(crate) enum TopLevelCommand {
    #[structopt(flatten)]
    Command(Command),
    /// Generate an auto-completion script for the selected shell
    Completions {
        #[structopt(case_insensitive = true, possible_values = &clap::Shell::variants())]
        shell: clap::Shell,
    },
}

pub(crate) fn parse_command_line(
    bin_name: &str,
    mut command_line: Vec<String>,
) -> Result<Option<Command>, clap::Error> {
    command_line.insert(0, bin_name.into());
    match TopLevelCommand::from_iter_safe(command_line)? {
        TopLevelCommand::Command(command) => Ok(Some(command)),
        TopLevelCommand::Completions { shell } => {
            TopLevelCommand::clap().gen_completions_to(bin_name, shell, &mut io::stdout().lock());
            Ok(None)
        }
    }
}

pub(crate) fn call_c_main<'a, T: TableWriter>(
    output: T::Output,
    config: &'a c_api::OwnedTomlValueFFI<'a>,
) {
    unsafe {
        let (output, vtable) = c_api::OutputFFI::from_output::<T>(output);
        c_api::c_main(output, vtable, config.as_ptr());
    }
}

pub(crate) fn call_c_main_with_transposed<'a, T: TableWriter>(
    output: T::Output,
    config: &'a c_api::OwnedTomlValueFFI<'a>,
    transposed: bool,
) {
    if transposed {
        call_c_main::<TransposedTableWriter<T>>(output, config);
    } else {
        call_c_main::<T>(output, config);
    }
}

#[cfg(target_arch = "wasm32")]
/// cbindgen:ignore
extern "C" {
    fn get_args_string_len() -> usize;
    fn get_args_string(text: *mut u8);
}

#[cfg(target_arch = "wasm32")]
fn get_command_line() -> Vec<String> {
    let mut command_line = vec![0u8; unsafe { get_args_string_len() }];
    unsafe {
        get_args_string(command_line.as_mut_ptr());
    }
    match shell_words::split(&String::from_utf8(command_line).unwrap()) {
        Ok(v) => v,
        Err(e) => {
            eprintln!("Error: {}", e);
            process::exit(1);
        }
    }
}

#[cfg(not(target_arch = "wasm32"))]
fn get_command_line() -> Vec<String> {
    std::env::args().skip(1).collect()
}

pub fn lib_main(bin_name: &str) {
    let command = match parse_command_line(bin_name, get_command_line()) {
        Ok(Some(v)) => v,
        Ok(None) => return,
        Err(e) => {
            if e.use_stderr() {
                e.exit();
            } else {
                println!("{}", e.message);
                return;
            }
        }
    };
    match command {
        Command::Run(c) => run_command(c),
        Command::DumpDefaultConfig(DumpDefaultConfigSubcommand {}) => {
            println!("{}", get_default_config_toml_source());
        }
    }
}

pub(crate) fn get_default_config_toml_source() -> &'static str {
    include_str!(concat!(env!("CARGO_MANIFEST_DIR"), "/default-config.toml"))
}

pub(crate) fn read_input_file(path: Option<impl AsRef<Path>>) -> io::Result<toml::Value> {
    if let Some(path) = path {
        let path = path.as_ref();
        let mut retval = String::new();
        if path == Path::new("-") {
            io::stdin().lock().read_to_string(&mut retval)?;
        } else if cfg!(target_arch = "wasm32") {
            return Err(io::Error::new(
                io::ErrorKind::NotFound,
                "file reading is not supported on wasm, use `-` to read from stdin instead",
            ));
        } else {
            retval = fs::read_to_string(path)?;
        }
        Ok(retval.parse()?)
    } else {
        Ok(get_default_config_toml_source()
            .parse()
            .expect("failed to parse default config toml"))
    }
}

pub(crate) fn run_command(command: RunSubcommand) {
    let config_toml = match read_input_file(command.config) {
        Ok(v) => v,
        Err(e) => {
            eprintln!("Error parsing config TOML: {}", e);
            process::exit(1);
        }
    };
    let mut config_ffi_builder = c_api::OwnedTomlValueFFIBuilder::new();
    let config = config_ffi_builder.build(&config_toml);
    let mut output = Vec::new();
    let output_type = command.output_type.unwrap_or_default();
    match output_type {
        OutputType::HTML => call_c_main_with_transposed::<table::HtmlTableWriter<_>>(
            &mut output,
            config,
            command.transpose,
        ),
        OutputType::Markdown => call_c_main_with_transposed::<table::MarkdownTableWriter<_>>(
            &mut output,
            config,
            command.transpose,
        ),
        OutputType::VCD => call_c_main_with_transposed::<table::VcdTableWriter<_>>(
            &mut output,
            config,
            command.transpose,
        ),
    }
    let mut output = String::from_utf8_lossy(&output);
    if command.pretty {
        match output_type {
            OutputType::Markdown => {
                output = Cow::Owned(reformat_markdown(&output));
            }
            OutputType::HTML | OutputType::VCD => {
                eprintln!("warning: --pretty option only supported for Markdown output.");
            }
        }
    }
    c_api::write_output(&output, output_type == OutputType::HTML);
}

fn reformat_markdown(text: &str) -> String {
    let arena = comrak::Arena::new();
    let options = comrak::ComrakOptions {
        extension: comrak::ComrakExtensionOptions {
            strikethrough: true,
            tagfilter: false,
            table: true,
            autolink: true,
            tasklist: true,
            superscript: false,
            header_ids: None,
            footnotes: true,
            description_lists: false,
        },
        parse: comrak::ComrakParseOptions::default(),
        render: comrak::ComrakRenderOptions::default(),
    };
    let root = comrak::parse_document(&arena, text, &options);
    let mut output = Vec::new();
    comrak::format_commonmark(root, &options, &mut output).unwrap();
    String::from_utf8(output).unwrap()
}
